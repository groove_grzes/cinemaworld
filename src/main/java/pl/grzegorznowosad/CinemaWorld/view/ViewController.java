package pl.grzegorznowosad.CinemaWorld.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.grzegorznowosad.CinemaWorld.controller.dto.*;
import pl.grzegorznowosad.CinemaWorld.model.MovieProjection;
import pl.grzegorznowosad.CinemaWorld.model.RoleApplication;
import pl.grzegorznowosad.CinemaWorld.service.MovieProjectionServiceImp;
import pl.grzegorznowosad.CinemaWorld.service.MovieServiceImp;
import pl.grzegorznowosad.CinemaWorld.service.RoomServiceImp;
import pl.grzegorznowosad.CinemaWorld.service.converter.*;
import pl.grzegorznowosad.CinemaWorld.service.exception.*;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.*;
import pl.grzegorznowosad.CinemaWorld.service.other.AccessToAdministrationValidator;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Set;


@Controller
public class ViewController {

    @Autowired
    private CityService cityService;

    @Autowired
    private MovieProjectionServiceImp movieProjectionServiceImp;

    @Autowired
    private MovieServiceImp movieServiceImp;

    @Autowired
    private RoomServiceImp roomServiceImp;

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private CityDtoConverter cityDtoConverter;

    @Autowired
    private RoomService roomService;

    @Autowired
    private RoomDtoConverter roomDtoConverter;

    @Autowired
    private MovieCategoryService movieCategoryService;

    @Autowired
    private MovieCategoryDtoConverter movieCategoryDtoConverter;

    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieProjectionService movieProjectionService;

    @Autowired
    private MovieProjectionCreateFromViewDtoConverter movieProjectionCreateFromViewDtoConverter;

    @Autowired
    private UserService userService;

    @Autowired
    private AccessToAdministrationValidator accessToAdministrationValidator;

    @Autowired
    private RoleApplicationService roleApplicationService;



    @RequestMapping("/loginpage")
    public ModelAndView toLoginPage(@RequestParam(required = false, name = "error") String error, @RequestParam(required = false, name = "logout") String logout){
        ModelAndView mav = new ModelAndView("loginPage");

        if(error != null){
            mav.addObject("error", "yes, there is an error ;-)");
        }

        if(logout != null){
            mav.addObject("logout", "tak, wylogowales sie");
        }

        return mav;
    }

    @RequestMapping("/")
    public ModelAndView toMain(Authentication auth) throws UserNotFound {
        ModelAndView mav = new ModelAndView("main");

        List<CityDto> allCities = cityService.getAll();
        mav.addObject("cities", allCities);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @RequestMapping("/city/{id}")
    public ModelAndView toCity(@PathVariable Integer id, Authentication auth) throws CityNotFound, UserNotFound {
        ModelAndView mav = new ModelAndView("city");

        List<MovieProjectionUIDto> movieProjectionUIDtos = movieProjectionServiceImp.allMovieProjectionsToUIByCity(id);
        mav.addObject("movieProjections", movieProjectionUIDtos);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @RequestMapping("/projection/{id}")
    public ModelAndView toProjection(@PathVariable Integer id, Authentication auth) throws MovieProjectionNotFound, RoomNotFound, MovieNotFound, UserNotFound {
        ModelAndView mav = new ModelAndView("projection");

        MovieProjectionDto byId = movieProjectionServiceImp.getById(id);

        RoomDto roomByName = roomServiceImp.getByName(byId.getRoomName());

        mav.addObject("date", byId.getDate());
        mav.addObject("timeFrom", byId.getTimeFrom());
        mav.addObject("timeTo", byId.getTimeTo());
        mav.addObject("roomName", byId.getRoomName());
        mav.addObject("movieTitle", byId.getMovieTitle());
        mav.addObject("rows", roomByName.getRows());
        mav.addObject("columns", roomByName.getSeats());

        mav.addObject("movieIdNumber", byId.getId());

        String name = auth.getName();

        mav.addObject("userLogin", name);

        ReservationWithStatusDto[][] allReservationsForMovieProjection = movieProjectionService.getAllReservationsForMovieProjection(byId.getId());
        mav.addObject("reservations", allReservationsForMovieProjection);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @CrossOrigin(origins = "true", allowedHeaders = "true", methods = RequestMethod.POST, allowCredentials = "true")
    @PostMapping("/projection/{id}/makeReservations")
    public ModelAndView getManyReservations(@PathVariable Integer id, @RequestBody ReservationPackFromViewDto reservationPackFromViewDto) throws UserNotFound, MovieProjectionNotFound, MessagingException {

        reservationService.saveNewReservations(reservationPackFromViewDto);

        ModelAndView mav = new ModelAndView("thankyou");

        return mav;
    }

    @RequestMapping("/success")
    public ModelAndView redirectWithSuccess(Authentication auth) throws UserNotFound {
        ModelAndView mav = new ModelAndView("thankyou");

        UserDto byLogin = userService.getByLogin(auth.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @RequestMapping("/oops")
    public ModelAndView redirectWithFail(){
        ModelAndView mav = new ModelAndView("notickets");

        return mav;
    }

    @GetMapping("/administration")
    public ModelAndView toAdministration(Authentication auth) throws UserNotFound {
        ModelAndView mav = new ModelAndView("administration");

        String userName = userService.getByLogin(auth.getName()).getName();

        mav.addObject("name", userName);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @GetMapping("/administration/city")
    public ModelAndView toAdministrationCity(Authentication auth) throws UserNotFound {
        ModelAndView mav = new ModelAndView("administration_city");

        List<CityDto> allCitiesList = cityService.getAll();

        mav.addObject("allCities", allCitiesList);

        CityUpdateDto newCity = new CityUpdateDto();

        mav.addObject("newCityModel", newCity);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @GetMapping("/administration/city/delete/{id}")
    public String deleteCityFromView(@PathVariable Integer id) throws CityNotFound {

        cityService.delete(id);

        return "redirect:/administration/city";
    }

    @PostMapping("/administration/city/create")
    public String createNewCity(@ModelAttribute(name = "newCityModel") CityUpdateDto cityUpdateDto) throws CityIncorrect, CityAlreadyExist {

        CityDto createdCity = cityService.create(cityUpdateDto);

        return "redirect:/administration/city";
    }

    @GetMapping("/administration/city/edit/{id}")
    public ModelAndView editCity(@PathVariable Integer id, Authentication auth) throws CityNotFound, UserNotFound {
        ModelAndView mav = new ModelAndView("administration_city_edit");

        CityDto cityById = cityService.getById(id);

        CityUpdateDto cityUpdateDto = cityDtoConverter.toUpdateDto(cityById);

        mav.addObject("cityToUpdate", cityUpdateDto);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @PostMapping("/administration/city/edit")
    public String updateCityInDB(@ModelAttribute(name = "cityToUpdate") CityUpdateDto cityUpdateDto) throws CityIncorrect, CityNotFound {

        cityService.update(cityUpdateDto.getId(), cityUpdateDto);

        return "redirect:/administration/city";
    }

    @GetMapping("/administration/room")
    public ModelAndView toAdministrationRoom(Authentication auth) throws UserNotFound {
        ModelAndView mav = new ModelAndView("administration_room");

        List<RoomDto> allRoomsInCinema = roomService.getAll();

        mav.addObject("allRooms", allRoomsInCinema);

        RoomCreateDto roomCreateDto = new RoomCreateDto();

        mav.addObject("newRoom", roomCreateDto);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @PostMapping("/administration/room/create")
    public String createNewRoomFromView(@ModelAttribute(name = "newRoom") RoomCreateDto roomCreateDto) throws RoomIncorrect {

        RoomDto roomDtoCreated = roomService.create(roomCreateDto);

        return "redirect:/administration/room";
    }

    @GetMapping("/administration/room/delete/{id}")
    public String deleteRoom(@PathVariable Integer id) throws RoomNotFound {

        RoomDto deletedRoom = roomService.delete(id);

        return "redirect:/administration/room";
    }

    @GetMapping("/administration/room/edit/{id}")
    public ModelAndView toUpdateRoom(@PathVariable Integer id, Authentication auth) throws RoomNotFound, UserNotFound {

        ModelAndView mav = new ModelAndView("administration_room_edit");

        RoomDto roomToBeUpdated = roomService.getById(id);

        mav.addObject("roomUpdate", roomToBeUpdated);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @PostMapping("/administration/room/edit")
    public String updateRoom(@ModelAttribute(name = "roomUpdate") RoomDto roomDto) throws RoomIncorrect, RoomNotFound {

        RoomCreateDto roomCreateDto = roomDtoConverter.toDtoFromVIew(roomDto);

        RoomDto updatedRoom = roomService.update(roomCreateDto.getId(), roomCreateDto);

        return "redirect:/administration/room";
    }

    @GetMapping("/administration/moviecategory")
    public ModelAndView toAdministrationMoviecategory(Authentication auth) throws UserNotFound {
        ModelAndView mav = new ModelAndView("administration_moviecategory");

        List<MovieCategoryDto> allMovieCategories = movieCategoryService.getAll();

        mav.addObject("movieCategories", allMovieCategories);

        MovieCategoryCreateDto movieCategoryCreateDto = new MovieCategoryCreateDto();

        mav.addObject("newMovieCategory", movieCategoryCreateDto);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @GetMapping("/administration/moviecategory/delete/{id}")
    public String deleteMovieCategory(@PathVariable Integer id) throws MovieCategoryNotFound {

        MovieCategoryDto deletedMovieCategory = movieCategoryService.delete(id);

        return "redirect:/administration/moviecategory";
    }

    @PostMapping("/administration/moviecategory/new")
    public String createNewMovieCategory(@ModelAttribute(name = "newMovieCategory") MovieCategoryCreateDto movieCategoryCreateDto) throws MovieCategoryIncorrect, MovieCategoryAleadyExist {

        MovieCategoryDto newMovieCategory = movieCategoryService.create(movieCategoryCreateDto);

        return "redirect:/administration/moviecategory";
    }

    @GetMapping("/administration/moviecategory/edit/{id}")
    public ModelAndView editMovieCategory(@PathVariable Integer id, Authentication auth) throws MovieCategoryNotFound, UserNotFound {
        ModelAndView mav = new ModelAndView("administration_moviecategory_edit");

        MovieCategoryDto movieCategoryById = movieCategoryService.getById(id);
        mav.addObject("movieCategory", movieCategoryById);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @PostMapping("/administration/moviecategory/edit")
    public String toEditMovieCategory(@ModelAttribute(name = "movieCategory") MovieCategoryDto movieCategoryDto) throws MovieCategoryIncorrect, MovieCategoryNotFound {

        MovieCategoryCreateDto movieCategoryCreateDto = movieCategoryDtoConverter.toCreateDtoView(movieCategoryDto);

        MovieCategoryDto updatedMovieCategory = movieCategoryService.update(movieCategoryDto.getId(), movieCategoryCreateDto);

        return "redirect:/administration/moviecategory";
    }

    @GetMapping("/administration/movie")
    public ModelAndView toAdministrationMovie(Authentication auth) throws UserNotFound {

        ModelAndView mav = new ModelAndView("administration_movie");

        List<MovieViewDto> allToView = movieService.getAllToView();

        mav.addObject("movies", allToView);

        MovieCreateViewDto newMovie = new MovieCreateViewDto();

        mav.addObject("movie", newMovie);

        List<MovieCategoryDto> allCategories = movieCategoryService.getAll();

        mav.addObject("categories", allCategories);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @GetMapping("/administration/movie/delete/{id}")
    public String deleteMovie(@PathVariable Integer id) throws MovieNotFound {

        MovieDto deletedMovie = movieService.delete(id);

        return "redirect:/administration/movie";
    }

    @PostMapping("/administration/movie/new")
    public String createMovie(@ModelAttribute(name = "movie") MovieCreateViewDto movieCreateViewDto) throws MovieIncorrect, MovieCategoryNotFound, MovieAlreadyExist {

        MovieCreateDto newMovie = new MovieCreateDto();
        newMovie.setYear(movieCreateViewDto.getYear());
        newMovie.setTitle(movieCreateViewDto.getTitle());
        newMovie.setDirector(movieCreateViewDto.getDirector());

        MovieCategoryDto movieCategoryByName = movieCategoryService.getByName(movieCreateViewDto.getCategory());

        newMovie.setCategoryId(movieCategoryByName.getId());


        MovieDto createdMovie = movieService.create(newMovie);

        return "redirect:/administration/movie";
    }

    @GetMapping("/administration/movie/edit/{id}")
    public ModelAndView toEditMovie(@PathVariable Integer id, Authentication auth) throws MovieNotFound, MovieCategoryNotFound, UserNotFound {

        ModelAndView mav = new ModelAndView("administration_movie_edit");

        MovieViewEditDto movieEdit = movieService.getToEditView(id);

        mav.addObject("movieToEdit", movieEdit);

        List<MovieCategoryDto> allCategories = movieCategoryService.getAll();

        mav.addObject("categories", allCategories);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @PostMapping("/administration/movie/edit")
    public String editMovie(@ModelAttribute(name = "movieToEdit") MovieViewEditDto movieViewEditDto) throws MovieCategoryNotFound, MovieIncorrect, MovieNotFound {

        MovieCreateDto movieEdit = new MovieCreateDto();
        movieEdit.setTitle(movieViewEditDto.getTitle());
        movieEdit.setDirector(movieViewEditDto.getDirector());
        movieEdit.setYear(movieViewEditDto.getYear());

        MovieCategoryDto categoryByName = movieCategoryService.getByName(movieViewEditDto.getMovieCategory());

        movieEdit.setCategoryId(categoryByName.getId());

        MovieDto editedMovie = movieService.update(movieViewEditDto.getId(), movieEdit);

        return "redirect:/administration/movie";
    }

    @GetMapping("/administration/movieprojection")
    public ModelAndView toMovieProjection(Authentication auth) throws UserNotFound {
        ModelAndView mav = new ModelAndView("administration_movieprojection");

        List<MovieProjectionDto> allMovieProjections = movieProjectionService.getAll();

        mav.addObject("movieProjections", allMovieProjections);

        MovieProjectionCreateDto movieProjectionCreateDto = new MovieProjectionCreateDto();

        mav.addObject("newMovieProjection", movieProjectionCreateDto);

        List<RoomDto> allRooms = roomService.getAll();
        List<MovieDto> allMovies = movieService.getAll();
        List<CityDto> allCities = cityService.getAll();

        mav.addObject("rooms", allRooms);
        mav.addObject("cities", allCities);
        mav.addObject("movies", allMovies);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @GetMapping("/administration/movieprojection/delete/{id}")
    public String toDeleveMovieProjection(@PathVariable Integer id) throws MovieProjectionNotFound {

        MovieProjectionDto deletedMoviePorojection = movieProjectionService.delete(id);

        return "redirect:/administration/movieprojection";
    }

    @PostMapping("/administration/movieprojection/new")
    public String createNewMovieProjection(@ModelAttribute(name = "newMovieProjection") MovieProjectionCreateDto movieProjectionCreateDto) throws RoomNotFound, CityNotFound, MovieNotFound, MovieProjectionIncorrect {

        MovieProjectionDto createdMovieProjection = movieProjectionService.create(movieProjectionCreateDto);

        return "redirect:/administration/movieprojection";
    }

    @GetMapping("/administration/movieprojection/edit/{id}")
    public ModelAndView toEditMovieProjection(@PathVariable Integer id, Authentication auth) throws MovieProjectionNotFound, CityNotFound, UserNotFound {
        ModelAndView mav = new ModelAndView("administration_movieprojection_edit");

        MovieProjectionDto movieProjectionById = movieProjectionService.getById(id);

        CityDto cityById = cityService.getById(movieProjectionById.getCityId());

        MovieProjectionCreateFromViewDto movieProjectionCreateFromViewDto = movieProjectionCreateFromViewDtoConverter.toEditDto(movieProjectionById, id, cityById.getName());

        mav.addObject("editedMovieProjection", movieProjectionCreateFromViewDto);

        List<RoomDto> allRooms = roomService.getAll();
        List<MovieDto> allMovies = movieService.getAll();
        List<CityDto> allCities = cityService.getAll();

        mav.addObject("rooms", allRooms);
        mav.addObject("cities", allCities);
        mav.addObject("movies", allMovies);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        return mav;
    }

    @PostMapping("/administration/movieprojection/edit")
    public String editMovieProjection(@ModelAttribute(name = "editedMovieProjection") MovieProjectionCreateFromViewDto movieProjectionCreateFromViewDto) throws MovieProjectionIncorrect, MovieProjectionNotFound, CityNotFound, RoomNotFound, MovieNotFound {

        MovieProjectionCreateDto movieProjectionCreateDto = movieProjectionCreateFromViewDtoConverter.toModel(movieProjectionCreateFromViewDto);

        MovieProjectionDto updatedMovieProjection = movieProjectionService.update(movieProjectionCreateFromViewDto.getId(), movieProjectionCreateDto);

        return "redirect:/administration/movieprojection";
    }

    @GetMapping("/registration")
    public ModelAndView registration(){
        ModelAndView mav = new ModelAndView("registration");

        UserCreateDto userCreateDto = new UserCreateDto();

        mav.addObject("newUser", userCreateDto);

        return mav;
    }

    @PostMapping("/registration")
    public String toRegister(@ModelAttribute(name = "newUser") UserCreateDto userCreateDto) throws RoleApplicationNotFound, UserAlreadyExist, UserIncorrect {

        userService.create(userCreateDto);

        return "redirect:/";
    }

    @GetMapping("/administration/user")
    public ModelAndView usersList(Authentication auth) throws UserNotFound, RoleApplicationNotFound {
        ModelAndView mav = new ModelAndView("administration_user");

        List<UserDto> allUsers = userService.getAll();

        mav.addObject("users", allUsers);

        UserDto byLogin = userService.getByLogin(auth.getName());

        mav.addObject("name", byLogin.getName());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        boolean isOwner = accessToAdministrationValidator.isOwner(byLogin.getRole());
        mav.addObject("isOwner", isOwner);



        RoleApplicationDto newCleanRole = new RoleApplicationDto();
        mav.addObject("newRole", newCleanRole);

        return mav;
    }

    @GetMapping("/administration/user/edit/{id}")
    public ModelAndView toEditUser(@PathVariable Integer id, Authentication auth) throws RoleApplicationNotFound, UserNotFound {
        ModelAndView mav = new ModelAndView("administration_user_edit");

        UserDto byLogin = userService.getByLogin(auth.getName());
        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        UserDto userById = userService.getById(id);

        mav.addObject("userToEdit", userById);

        List<RoleApplicationDto> allRolesWithoutOwner = roleApplicationService.getAllWithoutOwner();
        mav.addObject("allRoles", allRolesWithoutOwner);

        return mav;
    }

    @PostMapping("/administration/user/edit")
    public String editUser(@ModelAttribute(name = "userToEdit") UserDto userDto) throws UserNotFound, RoleApplicationNotFound, UserIncorrect {
        UserDto userById = userService.getById(userDto.getId());

        UserUpdateDto updatedUser = new UserUpdateDto();

        updatedUser.setName(userById.getName());
        updatedUser.setPassword(userById.getPassword());
        updatedUser.setRole(userDto.getRole());

        userService.update(userById.getId(), updatedUser);

        return "redirect:/administration/user";
    }

    @GetMapping("/administration/user/delete/{id}")
    public String deleteUser(@PathVariable Integer id) throws UserNotFound {

        userService.delete(id);

        return "redirect:/administration/user";
    }

    @PostMapping("/administration/user/changeRole")
    public String changeRoleForUser(@ModelAttribute(name = "newRole") RoleApplicationDto roleApplicationDto) throws UserNotFound, RoleApplicationNotFound {

        userService.changeRole(roleApplicationDto.getId(), roleApplicationDto.getRoleName());

        return "redirect:/administration/user";
    }

    @GetMapping("/accessdenied")
    public ModelAndView toAccessDenied(Authentication auth){
        ModelAndView mav = new ModelAndView("accessdenied");

        return mav;
    }

    private String getName(Authentication auth) throws UserNotFound {

        return userService.getByLogin(auth.getName()).getName();

    }

    @GetMapping("/profile")
    public ModelAndView toProfile(Authentication auth) throws UserNotFound {
        ModelAndView mav = new ModelAndView("profile");

        UserDto byLogin = userService.getByLogin(auth.getName());
//
        mav.addObject("name", byLogin.getName());
        mav.addObject("login", byLogin.getLogin());

        boolean canAccessAdmin = accessToAdministrationValidator.canAccessToAdministration(byLogin.getRole());
        mav.addObject("canAccessAdminPanel", canAccessAdmin);

        List<MovieProjectionWithReservationPerUser> projectionsByUserWithReservations = reservationService.getByUser(byLogin.getId());
        mav.addObject("projectionsWithReservations", projectionsByUserWithReservations);

        return mav;
    }
}
