package pl.grzegorznowosad.CinemaWorld.controller.interfaces;

import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieDto;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieAlreadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieCategoryNotFound;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieNotFound;

import java.util.List;

public interface MovieController {

    // getAll
    public List<MovieDto> getAll();

    // getById
    public MovieDto getById(Integer id) throws MovieNotFound;

    // getByTitle
    public MovieDto getByTitle(String title) throws MovieNotFound;

    // create
    public MovieDto create(MovieCreateDto movieCreateDto) throws MovieAlreadyExist, MovieIncorrect, MovieCategoryNotFound;

    // update
    public MovieDto update(Integer id, MovieCreateDto movieCreateDto) throws MovieIncorrect, MovieNotFound, MovieCategoryNotFound;

    // delete
    public MovieDto delete(Integer id) throws MovieNotFound;

}
