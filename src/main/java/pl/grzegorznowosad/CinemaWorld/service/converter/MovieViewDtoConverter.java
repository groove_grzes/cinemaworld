package pl.grzegorznowosad.CinemaWorld.service.converter;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieViewDto;
import pl.grzegorznowosad.CinemaWorld.model.Movie;

@Component
public class MovieViewDtoConverter {

    public MovieViewDto toDto(Movie movie){
        return new MovieViewDto(movie.getId(), movie.getTitle(), movie.getDirector(), movie.getYear(), movie.getMovieCategory());
    }

}
