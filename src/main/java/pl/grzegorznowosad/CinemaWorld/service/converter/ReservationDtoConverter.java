package pl.grzegorznowosad.CinemaWorld.service.converter;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationDto;
import pl.grzegorznowosad.CinemaWorld.model.MovieProjection;
import pl.grzegorznowosad.CinemaWorld.model.Reservation;
import pl.grzegorznowosad.CinemaWorld.model.User;

@Component
public class ReservationDtoConverter {

    public Reservation toModel(ReservationCreateDto reservationCreateDto, User user, MovieProjection movieProjection){
        return new Reservation(null, reservationCreateDto.getRow(), reservationCreateDto.getSeat(), user, movieProjection);
    }

    public ReservationDto toDto(Reservation reservation){
        return new ReservationDto(reservation.getId(), reservation.getRow(), reservation.getSeat(), reservation.getUser().getLogin(), reservation.getMovieProjection().getId());
    }

}
