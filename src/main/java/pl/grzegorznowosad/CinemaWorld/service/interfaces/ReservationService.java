package pl.grzegorznowosad.CinemaWorld.service.interfaces;

import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieProjectionWithReservationPerUser;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationPackFromViewDto;
import pl.grzegorznowosad.CinemaWorld.model.MovieProjection;
import pl.grzegorznowosad.CinemaWorld.model.Reservation;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieProjectionNotFound;
import pl.grzegorznowosad.CinemaWorld.service.exception.ReservationIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.ReservationNotFound;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserNotFound;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Set;

public interface ReservationService {

    // getAll
    public List<ReservationDto> getAll();

    // getById
    public ReservationDto getById(Integer id) throws ReservationNotFound;

    // create
    public ReservationDto create(ReservationCreateDto reservationCreateDto) throws ReservationIncorrect, UserNotFound, MovieProjectionNotFound;

    // update
    public ReservationDto update(Integer id, ReservationCreateDto reservationCreateDto) throws ReservationIncorrect, ReservationNotFound;

    // delete
    public ReservationDto delete(Integer id) throws ReservationNotFound;

    // create new reservations from UI
    public List<Reservation> saveNewReservations(ReservationPackFromViewDto reservationPackFromViewDto) throws MovieProjectionNotFound, UserNotFound, MessagingException;

    public List<MovieProjectionWithReservationPerUser> getByUser(Integer id) throws UserNotFound;
}
