package pl.grzegorznowosad.CinemaWorld.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoomCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoomDto;
import pl.grzegorznowosad.CinemaWorld.model.Room;
import pl.grzegorznowosad.CinemaWorld.repository.RoomRepository;
import pl.grzegorznowosad.CinemaWorld.service.converter.RoomDtoConverter;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoomIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoomNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.RoomService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RoomServiceImp implements RoomService {

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private RoomDtoConverter roomDtoConverter;


    @Override
    public List<RoomDto> getAll() {
        List<Room> all = roomRepository.findAll();
        List<RoomDto> allRoomsDto = new ArrayList<>();

        all.forEach(x -> {
            allRoomsDto.add(roomDtoConverter.toDto(x));
        });

        return allRoomsDto;
    }

    @Override
    public RoomDto getById(Integer id) throws RoomNotFound {
        Optional<Room> byId = roomRepository.findById(id);

        if(byId.isPresent()){
            Room room = byId.get();

            return roomDtoConverter.toDto(room);
        }else{
            throw new RoomNotFound();
        }
    }

    @Override
    public RoomDto getByName(String name) throws RoomNotFound {
        Optional<Room> byName = roomRepository.findByName(name);

        if(byName.isPresent()){
            Room room = byName.get();

            return roomDtoConverter.toDto(room);

        }else{
            throw new RoomNotFound();
        }
    }

    @Override
    public RoomDto create(RoomCreateDto roomCreateDto) throws RoomIncorrect {
        if(roomCreateDto.getName() == null || roomCreateDto.getRows() == 0 || roomCreateDto.getRows()  == null ||
                roomCreateDto.getSeats() == 0 || roomCreateDto.getSeats() == null){
            throw new RoomIncorrect();
        }

        Room roomSaved = roomRepository.save(roomDtoConverter.toModel(roomCreateDto));

        return roomDtoConverter.toDto(roomSaved);
    }

    @Override
    public RoomDto update(Integer id, RoomCreateDto roomCreateDto) throws RoomIncorrect, RoomNotFound {
        if(roomCreateDto.getName() == null || roomCreateDto.getRows() == 0 || roomCreateDto.getRows()  == null ||
                roomCreateDto.getSeats() == 0 || roomCreateDto.getSeats() == null){
            throw new RoomIncorrect();
        }

        Optional<Room> byId = roomRepository.findById(id);

        if(byId.isPresent()){
            Room room = byId.get();

            room.setName(roomCreateDto.getName());
            room.setRows(roomCreateDto.getRows());
            room.setSeats(roomCreateDto.getSeats());

            Room roomUpdated = roomRepository.save(room);


            return roomDtoConverter.toDto(roomUpdated);

        }else{
            throw new RoomNotFound();
        }
    }

    @Override
    public RoomDto delete(Integer id) throws RoomNotFound {

        Optional<Room> byId = roomRepository.findById(id);

        if(byId.isPresent()){
            Room room = byId.get();

            roomRepository.delete(room);

            return roomDtoConverter.toDto(room);
        }else{
            throw new RoomNotFound();
        }
    }
}
