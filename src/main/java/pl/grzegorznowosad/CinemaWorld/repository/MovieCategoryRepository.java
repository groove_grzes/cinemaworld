package pl.grzegorznowosad.CinemaWorld.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieCategoryDto;
import pl.grzegorznowosad.CinemaWorld.model.MovieCategory;

import java.util.List;
import java.util.Optional;

@Repository
public interface MovieCategoryRepository extends CrudRepository<MovieCategory, Integer> {

    List<MovieCategory> findAll();
    Optional<MovieCategory> findById(Integer id);
    Optional<MovieCategory> findByName(String name);

}
