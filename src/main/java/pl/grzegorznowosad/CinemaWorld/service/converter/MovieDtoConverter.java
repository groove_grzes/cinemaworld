package pl.grzegorznowosad.CinemaWorld.service.converter;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.CinemaWorld.controller.dto.*;
import pl.grzegorznowosad.CinemaWorld.model.Movie;
import pl.grzegorznowosad.CinemaWorld.model.MovieCategory;

@Component
public class MovieDtoConverter {

    public Movie toModel(MovieCreateDto movieCreateDto, MovieCategory movieCategory){
        return new Movie(null, movieCreateDto.getTitle(), movieCreateDto.getDirector(), movieCreateDto.getYear(), null, movieCategory);
    }

    public MovieDto toDto(Movie movie){
        return new MovieDto(movie.getId(), movie.getTitle(), movie.getDirector(), movie.getYear(), movie.getMovieCategory().getId());
    }

    public Movie toModelFromViewDto(MovieCreateViewDto movieCreateViewDto, MovieCategory movieCategory){
        return new Movie(null, movieCreateViewDto.getTitle(), movieCreateViewDto.getDirector(), movieCreateViewDto.getYear(), null, movieCategory);
    }
}
