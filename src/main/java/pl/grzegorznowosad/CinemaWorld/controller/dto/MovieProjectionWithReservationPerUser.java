package pl.grzegorznowosad.CinemaWorld.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.grzegorznowosad.CinemaWorld.model.MovieProjection;
import pl.grzegorznowosad.CinemaWorld.model.Reservation;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieProjectionWithReservationPerUser {

    private MovieProjectionUIDto movieProjection;
    private List<ReservationDto> reservations;

}
