package pl.grzegorznowosad.CinemaWorld.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieCategoryCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieCategoryDto;
import pl.grzegorznowosad.CinemaWorld.model.MovieCategory;
import pl.grzegorznowosad.CinemaWorld.repository.MovieCategoryRepository;
import pl.grzegorznowosad.CinemaWorld.service.converter.MovieCategoryDtoConverter;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieCategoryAleadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieCategoryIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieCategoryNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.MovieCategoryService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MovieCategoryServiceImp implements MovieCategoryService {

    @Autowired
    private MovieCategoryRepository movieCategoryRepository;

    @Autowired
    private MovieCategoryDtoConverter movieCategoryDtoConverter;

    @Override
    public List<MovieCategoryDto> getAll() {
        List<MovieCategory> all = movieCategoryRepository.findAll();
        List<MovieCategoryDto> allMovieCategoryDto = new ArrayList<>();

        all.forEach(x -> {
            allMovieCategoryDto.add(movieCategoryDtoConverter.toDto(x));
        });

        return allMovieCategoryDto;
    }

    @Override
    public MovieCategoryDto getById(Integer id) throws MovieCategoryNotFound {
        Optional<MovieCategory> byId = movieCategoryRepository.findById(id);

        if(byId.isPresent()){
            MovieCategory movieCategory = byId.get();

            return movieCategoryDtoConverter.toDto(movieCategory);

        }else{
            throw new MovieCategoryNotFound();
        }
    }

    @Override
    public MovieCategoryDto getByName(String name) throws MovieCategoryNotFound {
        Optional<MovieCategory> byName = movieCategoryRepository.findByName(name);

        if(byName.isPresent()){
            MovieCategory movieCategory = byName.get();

            return movieCategoryDtoConverter.toDto(movieCategory);

        }else{
            throw new MovieCategoryNotFound();
        }
    }

    @Override
    public MovieCategoryDto create(MovieCategoryCreateDto categoryCreateDto) throws MovieCategoryIncorrect, MovieCategoryAleadyExist {
        if(categoryCreateDto.getName() == null){
            throw new MovieCategoryIncorrect();
        }

        if(movieCategoryRepository.findByName(categoryCreateDto.getName()).isPresent()){
            throw new MovieCategoryAleadyExist();
        }

        MovieCategory movieCategorySaved = movieCategoryRepository.save(movieCategoryDtoConverter.toModel(categoryCreateDto));

        return movieCategoryDtoConverter.toDto(movieCategorySaved);
    }

    @Override
    public MovieCategoryDto update(Integer id, MovieCategoryCreateDto categoryCreateDto) throws MovieCategoryIncorrect, MovieCategoryNotFound {
        if(categoryCreateDto.getName() == null){
            throw new MovieCategoryIncorrect();
        }

        Optional<MovieCategory> byId = movieCategoryRepository.findById(id);

        if(byId.isPresent()){
            MovieCategory movieCategory = byId.get();

            movieCategory.setName(categoryCreateDto.getName());

            MovieCategory updatedMovieCategrory = movieCategoryRepository.save(movieCategory);

            return movieCategoryDtoConverter.toDto(updatedMovieCategrory);

        }else{
            throw new MovieCategoryNotFound();
        }
    }

    @Override
    public MovieCategoryDto delete(Integer id) throws MovieCategoryNotFound {
        Optional<MovieCategory> byId = movieCategoryRepository.findById(id);

        if(byId.isPresent()){
            MovieCategory movieCategory = byId.get();

            movieCategoryRepository.delete(movieCategory);

            return movieCategoryDtoConverter.toDto(movieCategory);

        }else{
            throw new MovieCategoryNotFound();
        }
    }
}
