package pl.grzegorznowosad.CinemaWorld.service.interfaces;

import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieCategoryCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieCategoryDto;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieCategoryAleadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieCategoryIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieCategoryNotFound;

import java.util.List;

public interface MovieCategoryService {

    // getAll
    public List<MovieCategoryDto> getAll();

    // getById
    public MovieCategoryDto getById(Integer id) throws MovieCategoryNotFound;

    // getByName
    public MovieCategoryDto getByName(String name) throws MovieCategoryNotFound;

    // create
    public MovieCategoryDto create(MovieCategoryCreateDto categoryCreateDto) throws MovieCategoryIncorrect, MovieCategoryAleadyExist;

    // update
    public MovieCategoryDto update(Integer id, MovieCategoryCreateDto categoryCreateDto) throws MovieCategoryIncorrect, MovieCategoryNotFound;

    // delete
    public MovieCategoryDto delete(Integer id) throws MovieCategoryNotFound;

}
