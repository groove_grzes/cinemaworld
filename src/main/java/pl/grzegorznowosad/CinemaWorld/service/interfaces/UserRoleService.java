package pl.grzegorznowosad.CinemaWorld.service.interfaces;

import pl.grzegorznowosad.CinemaWorld.controller.dto.UserRoleCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserRoleDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserRoleUpdateDto;
import pl.grzegorznowosad.CinemaWorld.model.UserRole;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserRoleAlreadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserRoleDataIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserRoleNotFound;

import java.util.List;

public interface UserRoleService {

    // getAll
    public List<UserRoleDto> getAll();

    // getById
    public UserRoleDto getById(Integer id) throws UserRoleNotFound;

    // getByName
    public UserRoleDto getByName(String name) throws UserRoleNotFound;

    // create
    public UserRoleDto create(UserRoleCreateDto userRoleCreateDto) throws UserRoleDataIncorrect, UserRoleAlreadyExist;

    // update
    public UserRoleDto update(Integer id, UserRoleUpdateDto userRoleUpdateDto) throws UserRoleNotFound;

    // delete
    public UserRoleDto deleteById(Integer id) throws UserRoleNotFound;

    // deleteByName
    public UserRoleDto deleteByName(String name) throws UserRoleNotFound;

}
