package pl.grzegorznowosad.CinemaWorld.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReservationPackFromViewDto {

    private List<ReservationFromViewDto> listOfReservations;
    private String userName;
    private Integer movieProjectionId;

}
