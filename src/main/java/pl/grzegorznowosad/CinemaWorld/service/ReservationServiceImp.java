package pl.grzegorznowosad.CinemaWorld.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.CinemaWorld.controller.dto.*;
import pl.grzegorznowosad.CinemaWorld.model.*;
import pl.grzegorznowosad.CinemaWorld.repository.MovieProjectionRepository;
import pl.grzegorznowosad.CinemaWorld.repository.ReservationRepository;
import pl.grzegorznowosad.CinemaWorld.repository.UserRepository;
import pl.grzegorznowosad.CinemaWorld.service.converter.MovieProjectionDtoConverter;
import pl.grzegorznowosad.CinemaWorld.service.converter.ReservationDtoConverter;
import pl.grzegorznowosad.CinemaWorld.service.converter.ReservationFromUIToModelConverter;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieProjectionNotFound;
import pl.grzegorznowosad.CinemaWorld.service.exception.ReservationIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.ReservationNotFound;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.ReservationService;

import javax.mail.MessagingException;
import java.util.*;

@Service
public class ReservationServiceImp implements ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private ReservationDtoConverter reservationDtoConverter;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MovieProjectionRepository movieProjectionRepository;

    @Autowired
    private ReservationFromUIToModelConverter reservationFromUIToModelConverter;

    @Autowired
    private EmailService emailService;

    @Autowired
    private MovieProjectionDtoConverter movieProjectionDtoConverter;

    
    @Override
    public List<ReservationDto> getAll() {
        Iterable<Reservation> all = reservationRepository.findAll();
        List<ReservationDto> allReservationsDto = new ArrayList<>();

        all.forEach(x -> {
            allReservationsDto.add(reservationDtoConverter.toDto(x));
        });

        return allReservationsDto;
    }

    @Override
    public ReservationDto getById(Integer id) throws ReservationNotFound {
        Optional<Reservation> byId = reservationRepository.findById(id);

        if(byId.isPresent()){
            Reservation reservation = byId.get();

            return reservationDtoConverter.toDto(reservation);
        }else{
            throw new ReservationNotFound();
        }
    }

    @Override
    public ReservationDto create(ReservationCreateDto reservationCreateDto) throws ReservationIncorrect, UserNotFound, MovieProjectionNotFound {
        if(reservationCreateDto.getRow() == null || reservationCreateDto.getSeat() == null){
            throw new ReservationIncorrect();
        }

        Optional<User> userByLogin = userRepository.findByLogin(reservationCreateDto.getUser());
        Optional<MovieProjection> movieProjectionRepositoryById = movieProjectionRepository.findById(reservationCreateDto.getMovieProjectionId());

        if(userByLogin.isPresent()){

            if(movieProjectionRepositoryById.isPresent()){

                User user = userByLogin.get();
                MovieProjection movieProjection = movieProjectionRepositoryById.get();

                Reservation reservationSaved = reservationRepository.save(reservationDtoConverter.toModel(reservationCreateDto, user, movieProjection));

                return reservationDtoConverter.toDto(reservationSaved);

            }else{
                throw new MovieProjectionNotFound();
            }

        }else{
            throw new UserNotFound();
        }
    }

    @Override
    public ReservationDto update(Integer id, ReservationCreateDto reservationCreateDto) throws ReservationIncorrect, ReservationNotFound {
        if(reservationCreateDto.getRow() == null || reservationCreateDto.getSeat() == null){
            throw new ReservationIncorrect();
        }

        Optional<Reservation> byId = reservationRepository.findById(id);

        if(byId.isPresent()){
            Reservation reservation = byId.get();

            reservation.setRow(reservationCreateDto.getRow());
            reservation.setSeat(reservationCreateDto.getSeat());

            Reservation updatedReservation = reservationRepository.save(reservation);

            return reservationDtoConverter.toDto(updatedReservation);
        }else{
            throw new ReservationNotFound();
        }
    }

    @Override
    public ReservationDto delete(Integer id) throws ReservationNotFound {
        Optional<Reservation> byId = reservationRepository.findById(id);

        if(byId.isPresent()){
            Reservation reservation = byId.get();

            reservationRepository.delete(reservation);

            return reservationDtoConverter.toDto(reservation);
        }else{
            throw new ReservationNotFound();
        }
    }

    @Override
    public List<Reservation> saveNewReservations(ReservationPackFromViewDto reservationPackFromViewDto) throws MovieProjectionNotFound, UserNotFound, MessagingException {

        Optional<User> userByLogin = userRepository.findByLogin(reservationPackFromViewDto.getUserName());
        Optional<MovieProjection> movieProjectionRepositoryById = movieProjectionRepository.findById(reservationPackFromViewDto.getMovieProjectionId());

        List<ReservationFromViewDto> listOfReservations = reservationPackFromViewDto.getListOfReservations();

        if(userByLogin.isPresent()){
            if(movieProjectionRepositoryById.isPresent()){

                User user = userByLogin.get();
                MovieProjection movieProjection = movieProjectionRepositoryById.get();

                List<Reservation> listToBeSaved = new ArrayList<>();

                listOfReservations.forEach(reservation -> {
                    listToBeSaved.add(reservationFromUIToModelConverter.toModel(reservation, user, movieProjection));
                });

                List<Reservation> reservations = (List<Reservation>) reservationRepository.saveAll(listToBeSaved);
                System.out.println("zapisano: " + listToBeSaved);

                sendEmail(reservations, reservations.get(0).getUser().getLogin(), reservations.get(0).getUser().getName());

                return reservations;

            }else{
                throw new MovieProjectionNotFound();
            }
        }else{
            throw new UserNotFound();
        }
    }

    public void sendEmail(List<Reservation> listOfSavedReservations, String mail, String name) throws MessagingException {

        StringBuilder sb = new StringBuilder();
        sb.append("<h1><b>Cinema World</b></h1>Hello " + name + ", <br/><br/>Below You can find information about Your tickets:<ul>");

        listOfSavedReservations.forEach(reservation -> {
            sb.append("<li>row: " + (reservation.getRow() + 1)  + ", seat: " + (reservation.getSeat() + 1) + "</li>");
        });

        sb.append("</ul><br/>P.S.<br/>It's really cool that You check my app.<br/>For more information about me please visit <a href=\"http://www.grzegorznowosad.pl\">grzegorznowosad.pl</a><br/><br/>Thanks,<br/>Grzesiek");

        emailService.sendSimpleMessage(sb.toString(), mail);
    }

    public List<MovieProjectionWithReservationPerUser> getByUser(Integer id) throws UserNotFound {

        Optional<User> userById = userRepository.findById(id);

        if(userById.isPresent()){

            User user = userById.get();

            List<Reservation> allReservationByUser = reservationRepository.findAllByUser(user);

            Set<MovieProjection> movieProjectionSet = new HashSet<MovieProjection>();

            allReservationByUser.forEach(x -> {
                if(!movieProjectionSet.contains(x.getMovieProjection())){
                    movieProjectionSet.add(x.getMovieProjection());
                }
            });

            List<MovieProjectionWithReservationPerUser> projectionsWithReservationsPerUser = new ArrayList<>();

            movieProjectionSet.forEach(x -> {
                MovieProjectionWithReservationPerUser projectionWithReservation = new MovieProjectionWithReservationPerUser();
                projectionWithReservation.setMovieProjection(movieProjectionDtoConverter.toUIDto(x));
                projectionWithReservation.setReservations(new ArrayList<ReservationDto>());

                allReservationByUser.forEach(y -> {
                    if(y.getMovieProjection().equals(x)){
                        projectionWithReservation.getReservations().add(reservationDtoConverter.toDto(y));
                    }
                });

                projectionsWithReservationsPerUser.add(projectionWithReservation);

            });

            return projectionsWithReservationsPerUser;
        }else{
            throw new UserNotFound();
        }
    }
}
