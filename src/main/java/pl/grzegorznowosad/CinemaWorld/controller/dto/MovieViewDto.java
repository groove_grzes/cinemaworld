package pl.grzegorznowosad.CinemaWorld.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import pl.grzegorznowosad.CinemaWorld.model.MovieCategory;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieViewDto {

    private Integer id;
    private String title;
    private String director;
    private Integer year;
    private MovieCategory movieCategory;

}
