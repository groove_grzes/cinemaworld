package pl.grzegorznowosad.CinemaWorld.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.grzegorznowosad.CinemaWorld.model.Room;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoomRepository extends CrudRepository<Room, Integer> {

    List<Room> findAll();
    Optional<Room> findById(Integer id);
    Optional<Room> findByName(String name);

}
