package pl.grzegorznowosad.CinemaWorld.security;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import pl.grzegorznowosad.CinemaWorld.model.RoleApplication;
import pl.grzegorznowosad.CinemaWorld.service.exception.*;

@ControllerAdvice
public class ControllerConfig {

    @ExceptionHandler({
        CityAlreadyExist.class, CityIncorrect.class,CityNotFound.class,MovieAlreadyExist.class, MovieCategoryAleadyExist.class, MovieCategoryIncorrect.class, MovieCategoryNotFound.class, MovieIncorrect.class, MovieNotFound.class, MovieProjectionAlreadyExist.class, MovieProjectionIncorrect.class, MovieProjectionNotFound.class, ReservationAlreadyExist.class, ReservationIncorrect.class, ReservationNotFound.class, RoleApplicationAlreadyExists.class, RoleApplicationDataIncorrect.class, RoleApplicationNotFound.class, RoomAlreadyExist.class, RoomIncorrect.class, RoomNotFound.class, RoomNotFound.class, UserAlreadyExist.class, UserIncorrect.class, UserNotFound.class, UserRoleAlreadyExist.class, UserRoleDataIncorrect.class, UserRoleNotFound.class
    })
    public ModelAndView exceptionHandling(Exception e){
        ModelAndView mav = new ModelAndView("exception");

        mav.addObject("errorCode", e.getClass().getSimpleName());

        return mav;
    }
}
