function validate(){
    // var regex = new RegExp('^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$');
    var regex = new RegExp('[A-Z]aa');

    var regExMail = new RegExp('[^@]+@[^\\.]+\\..+');
    var regExPassword = new RegExp('^(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$');
    var regExName = new RegExp('[A-Za-z]{2,}');

    var username = document.getElementById("login").value;
    var usernameRes = regExMail.test(username);

    var password = document.getElementById("password").value;
    var passwordRes = regExPassword.test(password);

    var name = document.getElementById("name").value;
    var nameRes = regExName.test(name);

    var wynik = '';

    if(!usernameRes){
        wynik += 'login has to be email, where Your tickets will be send \n\n';
    }

    if(!passwordRes){
        wynik += 'password has to be minimum 8 characters, 1 big letter, 1 small letter, 1 number, 1 special character\n\n';
    }

    if(!nameRes){
        wynik += 'name has to be minimum 2 letters';
    }

    if(!wynik == ''){
        alert(wynik);

        return false;
    }
}