package pl.grzegorznowosad.CinemaWorld.controller.interfaces;

import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationDto;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieProjectionNotFound;
import pl.grzegorznowosad.CinemaWorld.service.exception.ReservationIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.ReservationNotFound;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserNotFound;

import java.util.List;

public interface ReservationController {

    // getAll
    public List<ReservationDto> getAll();

    // getById
    public ReservationDto getById(Integer id) throws ReservationNotFound;

    // create
    public ReservationDto create(ReservationCreateDto reservationCreateDto) throws ReservationIncorrect, UserNotFound, MovieProjectionNotFound;

    // update
    public ReservationDto update(Integer id, ReservationCreateDto reservationCreateDto) throws ReservationIncorrect, ReservationNotFound;

    // delete
    public ReservationDto delete(Integer id) throws ReservationNotFound;

}
