package pl.grzegorznowosad.CinemaWorld.repository;

import org.springframework.data.repository.CrudRepository;
import pl.grzegorznowosad.CinemaWorld.model.RoleApplication;

import java.util.List;
import java.util.Optional;

public interface RoleApplicationRepository extends CrudRepository<RoleApplication, Integer> {

    List<RoleApplication> findAll();
    Optional<RoleApplication> findById(Integer id);
    Optional<RoleApplication> findByRoleName(String roleName);
}
