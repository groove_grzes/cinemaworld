package pl.grzegorznowosad.CinemaWorld.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private MyUserDetails myUserDetails;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception{
        DaoAuthenticationProvider dap = new DaoAuthenticationProvider();

        dap.setUserDetailsService(myUserDetails);
        dap.setPasswordEncoder(new BCryptPasswordEncoder());

        auth.authenticationProvider(dap);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .csrf()
                .disable()
                .cors()
                .disable()
                .authorizeRequests()
                .antMatchers("/registration").permitAll()
                .antMatchers(HttpMethod.GET,"/bootstrap/**").permitAll()
                .antMatchers(HttpMethod.GET,"/css/**").permitAll()
                .antMatchers(HttpMethod.GET,"/js/**").permitAll()
                .antMatchers(HttpMethod.GET,"/pic/**").permitAll()
                .antMatchers("/administration/**").hasAnyRole("ADMIN", "OWNER")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                    .loginPage("/loginpage")
                    .loginProcessingUrl("/loginAuthenticate")
                    .permitAll()
                .and()
                .logout()
                .logoutSuccessUrl("/")
                .and()
                .exceptionHandling().accessDeniedPage("/accessdenied");

    }
}
