package pl.grzegorznowosad.CinemaWorld.service.converter;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserCreateDto;
import pl.grzegorznowosad.CinemaWorld.model.Reservation;
import pl.grzegorznowosad.CinemaWorld.model.RoleApplication;
import pl.grzegorznowosad.CinemaWorld.model.User;

import java.util.List;

@Component
public class UserCreateDtoConverter {

    public User toModel(UserCreateDto userCreateDto, RoleApplication roleApplication){
        return new User(null, userCreateDto.getLogin(), userCreateDto.getPassword(), userCreateDto.getName(), null, roleApplication);
    }

}
