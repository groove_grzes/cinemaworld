var lista = document.getElementById("lista");

var divWrapperMain = document.createElement("div");
divWrapperMain.setAttribute("id", "main-wrapper");
lista.appendChild(divWrapperMain);

for (var i = 0; i < citiesToDisplay.length; i++) {
    var nazwaMiasta = citiesToDisplay[i].name;
    var idMiasta = citiesToDisplay[i].id;

    var nowyElementListy = document.createElement("li");
    nowyElementListy.setAttribute("class", "box");
    divWrapperMain.appendChild(nowyElementListy);

    var nowyLink = document.createElement("a");

    nowyLink.href = "/city/" + idMiasta;

    nowyElementListy.append(nowyLink);

    var nowyDiv = document.createElement("div");
    nowyDiv.setAttribute("class", "box-item");
    nowyDiv.innerHTML = nazwaMiasta;
    nowyLink.append(nowyDiv);
}