package pl.grzegorznowosad.CinemaWorld.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.CinemaWorld.model.Mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender emailSender;

    private final String FROM = "cinema.world.service@gmail.com";
    private String TO = "";
    private final String TOPIC = "Cinema World tickets";

    public void sendSimpleMessage(String contentToBeSend, String name) throws MessagingException {

        TO = name;

        MimeMessage mimeMessage = emailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, false, "utf-8");

        mimeMessage.setContent(contentToBeSend, "text/html");
        mimeMessageHelper.setSubject(TOPIC);
        mimeMessageHelper.setTo(TO);
        mimeMessageHelper.setFrom(FROM);

        emailSender.send(mimeMessage);
    }

}



