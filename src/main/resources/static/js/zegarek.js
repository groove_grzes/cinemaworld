countdown("zegarek", 1,30);

var divZegarek = document.getElementById("zegarek");

function countdown(elementName, minutes, seconds){

    var element, endTime, hours, mins, msLeft, time;
    var naKoniec = document.getElementById("TimeIsUp")

    element = document.getElementById(elementName);
    endTime = (+new Date) + 1000 * (60*minutes + seconds) + 500;

    updateTimer();

    function twoDigits( n ){
        return (n <= 9 ? "0" + n : n);
    }

    function updateTimer(){
        msLeft = endTime - (+new Date);
        if ( msLeft < 1000 ) {

            // element.innerHTML = "Time is up! If You still want to refresh page, click " + "<a href='" + aaa+ "'>here</a>";
            naKoniec.innerHTML = "Time is up! If You still want to buy tickets, You need to refresh page, click " + "<a href='javascript:location.reload(true);'>here</a>";
            document.getElementById("main2").style.display = "none";
            naKoniec.style.display = "inline";
        } else {

            if(msLeft < 60000){
                document.getElementById("zegarek").style.backgroundColor = "orange";
            }

            if(msLeft < 10000){
                document.getElementById("zegarek").style.backgroundColor = "red";
            }

            time = new Date( msLeft );
            hours = time.getUTCHours();
            mins = time.getUTCMinutes();
            element.innerHTML = (hours ? hours + ':' + twoDigits( mins ) : mins) + ':' + twoDigits( time.getUTCSeconds() );
            setTimeout( updateTimer, time.getUTCMilliseconds() + 500 );
        }
    }
}


