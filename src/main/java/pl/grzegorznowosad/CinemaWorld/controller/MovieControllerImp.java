package pl.grzegorznowosad.CinemaWorld.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieDto;
import pl.grzegorznowosad.CinemaWorld.controller.interfaces.MovieController;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieAlreadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieCategoryNotFound;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.MovieService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class MovieControllerImp implements MovieController {

    @Autowired
    private MovieService movieService;

    @Override
    @GetMapping("movie")
    public List<MovieDto> getAll() {
        return movieService.getAll();
    }

    @Override
    @GetMapping("movie/{id}")
    public MovieDto getById(@PathVariable Integer id) throws MovieNotFound {
        return movieService.getById(id);
    }

    @Override
    @GetMapping("movie/title/{title}")
    public MovieDto getByTitle(@PathVariable String title) throws MovieNotFound {
        return movieService.getByTitle(title);
    }

    @Override
    @PostMapping("movie")
    public MovieDto create(@RequestBody MovieCreateDto movieCreateDto) throws MovieAlreadyExist, MovieIncorrect, MovieCategoryNotFound {
        return movieService.create(movieCreateDto);
    }

    @Override
    @PutMapping("movie/{id}")
    public MovieDto update(@PathVariable Integer id, @RequestBody MovieCreateDto movieCreateDto) throws MovieIncorrect, MovieNotFound, MovieCategoryNotFound {
        return movieService.update(id, movieCreateDto);
    }

    @Override
    @DeleteMapping("movie/{id}")
    public MovieDto delete(@PathVariable Integer id) throws MovieNotFound {
        return movieService.delete(id);
    }
}
