package pl.grzegorznowosad.CinemaWorld.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserRoleCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserRoleDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserRoleUpdateDto;
import pl.grzegorznowosad.CinemaWorld.model.UserRole;
import pl.grzegorznowosad.CinemaWorld.repository.UserRoleRepository;
import pl.grzegorznowosad.CinemaWorld.service.converter.UserRoleConverterDto;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserRoleAlreadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserRoleDataIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserRoleNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.UserRoleService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserRoleServiceImp  implements UserRoleService {

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserRoleConverterDto userRoleConverterDto;

    @Override
    public List<UserRoleDto> getAll() {
        List<UserRole> allUserRoles = userRoleRepository.findAll();
        List<UserRoleDto> allUserRoleDto = new ArrayList<>();

        allUserRoles.forEach(userRole -> {
            allUserRoleDto.add(userRoleConverterDto.toDto(userRole));
        });

        return allUserRoleDto;
    }

    @Override
    public UserRoleDto getById(Integer id) throws UserRoleNotFound {
        Optional<UserRole> userRolebyId = userRoleRepository.findById(id);

        if(userRolebyId.isPresent()){

            UserRole userRole = userRolebyId.get();

            return userRoleConverterDto.toDto(userRole);

        }else{
            throw new UserRoleNotFound();
        }
    }

    @Override
    public UserRoleDto getByName(String name) throws UserRoleNotFound {
        Optional<UserRole> userRoleByName = userRoleRepository.findByRole(name);

        if(userRoleByName.isPresent()){

            UserRole userRole = userRoleByName.get();

            return userRoleConverterDto.toDto(userRole);

        }else{
            throw new UserRoleNotFound();
        }
    }

    @Override
    public UserRoleDto create(UserRoleCreateDto userRoleCreateDto) throws UserRoleDataIncorrect, UserRoleAlreadyExist {
        if(userRoleCreateDto.getRole() == null || userRoleCreateDto.getRole().isEmpty()){
            throw new UserRoleDataIncorrect();
        }

        if(userRoleRepository.findByRole(userRoleCreateDto.getRole()).isPresent()){
            throw new UserRoleAlreadyExist();
        }

        UserRole saveUserRole = userRoleRepository.save(userRoleConverterDto.toModel(userRoleCreateDto));

        return userRoleConverterDto.toDto(saveUserRole);
    }

    @Override
    public UserRoleDto update(Integer id, UserRoleUpdateDto userRoleUpdateDto) throws UserRoleNotFound {
        if(userRoleUpdateDto.getRole() == null || userRoleUpdateDto.getRole().isEmpty()){
            throw new UserRoleNotFound();
        }

        Optional<UserRole> userRoleById = userRoleRepository.findById(id);

        if(userRoleById.isPresent()){

            UserRole userRole = userRoleById.get();

            userRole.setRole(userRoleUpdateDto.getRole());

            UserRole updatedUserRole = userRoleRepository.save(userRole);

            return userRoleConverterDto.toDto(updatedUserRole);

        }else{
            throw new UserRoleNotFound();
        }
    }

    @Override
    public UserRoleDto deleteById(Integer id) throws UserRoleNotFound {
        Optional<UserRole> userRoleById = userRoleRepository.findById(id);

        if(userRoleById.isPresent()){

            UserRole userRole = userRoleById.get();

            userRoleRepository.delete(userRole);

            return userRoleConverterDto.toDto(userRole);

        }else{
            throw new UserRoleNotFound();
        }
    }

    @Override
    public UserRoleDto deleteByName(String name) throws UserRoleNotFound {
        Optional<UserRole> userRoleByName = userRoleRepository.findByRole(name);

        if(userRoleByName.isPresent()){

            UserRole userRole = userRoleByName.get();

            userRoleRepository.delete(userRole);

            return userRoleConverterDto.toDto(userRole);

        }else{
            throw new UserRoleNotFound();
        }
    }
}
