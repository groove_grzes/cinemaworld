package pl.grzegorznowosad.CinemaWorld.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserRoleCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserRoleDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserRoleUpdateDto;
import pl.grzegorznowosad.CinemaWorld.controller.interfaces.UserRoleController;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserRoleAlreadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserRoleDataIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserRoleNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.UserRoleService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class UserRoleControllerImp implements UserRoleController {

    @Autowired
    private UserRoleService userRoleService;

    @Override
    @GetMapping("userRole")
    public List<UserRoleDto> getAll() {
        return userRoleService.getAll();
    }

    @Override
    @GetMapping("userRole/{id}")
    public UserRoleDto getById(@PathVariable Integer id) throws UserRoleNotFound {
        return userRoleService.getById(id);
    }

    @Override
    @GetMapping("userRole/name/{name}")
    public UserRoleDto getByName(@PathVariable String name) throws UserRoleNotFound {
        return userRoleService.getByName(name);
    }

    @Override
    @PostMapping("userRole")
    public UserRoleDto create(@RequestBody UserRoleCreateDto userRoleCreateDto) throws UserRoleDataIncorrect, UserRoleAlreadyExist {
        return userRoleService.create(userRoleCreateDto);
    }

    @Override
    @PutMapping("userRole/{id}")
    public UserRoleDto update(@PathVariable Integer id, @RequestBody UserRoleUpdateDto userRoleUpdateDto) throws UserRoleNotFound {
        return userRoleService.update(id, userRoleUpdateDto);
    }

    @Override
    @DeleteMapping("userRole/{id}")
    public UserRoleDto deleteById(@PathVariable Integer id) throws UserRoleNotFound {
        return userRoleService.deleteById(id);
    }

    @Override
    @DeleteMapping("userRole/delete/{name}")
    public UserRoleDto deleteByName(@PathVariable String name) throws UserRoleNotFound {
        return userRoleService.deleteByName(name);
    }
}
