package pl.grzegorznowosad.CinemaWorld.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.CinemaWorld.controller.dto.*;
import pl.grzegorznowosad.CinemaWorld.model.Movie;
import pl.grzegorznowosad.CinemaWorld.model.MovieCategory;
import pl.grzegorznowosad.CinemaWorld.repository.MovieCategoryRepository;
import pl.grzegorznowosad.CinemaWorld.repository.MovieRepository;
import pl.grzegorznowosad.CinemaWorld.service.converter.MovieCategoryDtoConverter;
import pl.grzegorznowosad.CinemaWorld.service.converter.MovieDtoConverter;
import pl.grzegorznowosad.CinemaWorld.service.converter.MovieViewDtoConverter;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieAlreadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieCategoryNotFound;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.MovieCategoryService;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.MovieService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImp implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieDtoConverter movieDtoConverter;

    @Autowired
    private MovieViewDtoConverter movieViewDtoConverter;

    @Autowired
    private MovieCategoryService movieCategoryService;

    @Autowired
    private MovieCategoryDtoConverter movieCategoryDtoConverter;

    @Autowired
    private MovieCategoryRepository movieCategoryRepository;

    @Override
    public List<MovieDto> getAll() {
        List<Movie> all = movieRepository.findAll();
        List<MovieDto> allMoviesDto = new ArrayList<>();

        all.forEach(x -> {
            allMoviesDto.add(movieDtoConverter.toDto(x));
        });

        return allMoviesDto;
    }

    @Override
    public MovieDto getById(Integer id) throws MovieNotFound {
        Optional<Movie> byId = movieRepository.findById(id);

        if(byId.isPresent()){
            Movie movie = byId.get();

            return movieDtoConverter.toDto(movie);
        }else{
            throw new MovieNotFound();
        }
    }

    @Override
    public MovieDto getByTitle(String title) throws MovieNotFound {
        Optional<Movie> byTitle = movieRepository.findByTitle(title);

        if(byTitle.isPresent()){
            Movie movie = byTitle.get();

            return movieDtoConverter.toDto(movie);
        }else{
            throw new MovieNotFound();
        }
    }

    @Override
    public MovieDto create(MovieCreateDto movieCreateDto) throws MovieIncorrect, MovieAlreadyExist, MovieCategoryNotFound {
        if(movieCreateDto.getDirector().isEmpty() || movieCreateDto.getDirector() == null ||
                movieCreateDto.getTitle().isEmpty() || movieCreateDto.getDirector() == null ||
                movieCreateDto.getYear() == null){
            throw new MovieIncorrect();
        }

        if(movieRepository.findByTitle(movieCreateDto.getTitle()).isPresent()){
            throw new MovieAlreadyExist();
        }

        Optional<MovieCategory> categoryById = movieCategoryRepository.findById(movieCreateDto.getCategoryId());

        if(categoryById.isPresent()){

            MovieCategory movieCategory = categoryById.get();

            Movie movieSaved = movieRepository.save(movieDtoConverter.toModel(movieCreateDto, movieCategory));

            return movieDtoConverter.toDto(movieSaved);

        }else{
            throw new MovieCategoryNotFound();
        }
    }

    @Override
    public MovieDto update(Integer id, MovieCreateDto movieCreateDto) throws MovieIncorrect, MovieNotFound, MovieCategoryNotFound {
        if(movieCreateDto.getDirector().isEmpty() || movieCreateDto.getDirector() == null ||
                movieCreateDto.getTitle().isEmpty() || movieCreateDto.getDirector() == null ||
                movieCreateDto.getYear() == null){
            throw new MovieIncorrect();
        }

        Optional<Movie> byId = movieRepository.findById(id);
        Optional<MovieCategory> categoryById = movieCategoryRepository.findById(movieCreateDto.getCategoryId());

        if(byId.isPresent()){

            if(categoryById.isPresent()){

                Movie movie = byId.get();
                MovieCategory movieCategory = categoryById.get();

                movie.setDirector(movieCreateDto.getDirector());
                movie.setTitle(movieCreateDto.getTitle());
                movie.setYear(movieCreateDto.getYear());
                movie.setMovieCategory(movieCategory);

                Movie movieUpdated = movieRepository.save(movie);

                return movieDtoConverter.toDto(movieUpdated);

            }else{
                throw new MovieCategoryNotFound();
            }
        }else{
            throw new MovieNotFound();
        }

    }

    @Override
    public MovieDto delete(Integer id) throws MovieNotFound {
        Optional<Movie> byId = movieRepository.findById(id);

        if(byId.isPresent()){
            Movie movie = byId.get();

            movieRepository.delete(movie);

            return movieDtoConverter.toDto(movie);
        }else{
            throw new MovieNotFound();
        }
    }

    @Override
    public List<MovieViewDto> getAllToView() {
        List<Movie> allMovies = movieRepository.findAll();

        List<MovieViewDto> allMoviesViewDto = new ArrayList<>();

        allMovies.forEach(movie -> {
            allMoviesViewDto.add(movieViewDtoConverter.toDto(movie));
        });

        return allMoviesViewDto;
    }


    @Override
    public MovieViewEditDto getToEditView(Integer id) throws MovieNotFound, MovieCategoryNotFound {
        MovieDto movieById = getById(id);

        MovieCategoryDto movieCategoryById = movieCategoryService.getById(movieById.getMovieCategoryId());

        return new MovieViewEditDto(movieById.getId(), movieById.getTitle(), movieById.getDirector(), movieById.getYear(), movieCategoryById.getName());
    }
}
