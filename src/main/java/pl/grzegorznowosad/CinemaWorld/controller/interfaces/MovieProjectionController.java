package pl.grzegorznowosad.CinemaWorld.controller.interfaces;

import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieProjectionCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieProjectionDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationWithStatusDto;
import pl.grzegorznowosad.CinemaWorld.service.exception.*;

import java.util.List;

public interface MovieProjectionController {

    // getAll
    public List<MovieProjectionDto> getAll();

    // getById
    public MovieProjectionDto getById(Integer id) throws MovieProjectionNotFound;

    // create
    public MovieProjectionDto create(MovieProjectionCreateDto movieProjectionCreateDto) throws MovieProjectionIncorrect, CityNotFound, RoomNotFound, MovieNotFound;

    // update
    public MovieProjectionDto update(Integer id, MovieProjectionCreateDto movieProjectionCreateDto) throws MovieProjectionIncorrect, MovieProjectionNotFound, CityNotFound, RoomNotFound, MovieNotFound;

    // delete
    public MovieProjectionDto delete(Integer id) throws MovieProjectionNotFound;

    // getAllReservations
    public ReservationWithStatusDto[][] getAllReservations(Integer id) throws MovieProjectionNotFound;

}
