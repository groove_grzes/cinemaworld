package pl.grzegorznowosad.CinemaWorld.controller.interfaces;

import pl.grzegorznowosad.CinemaWorld.controller.dto.CityDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.CityUpdateDto;
import pl.grzegorznowosad.CinemaWorld.service.exception.CityAlreadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.CityIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.CityNotFound;

import java.util.List;

public interface CityController {

    // getAll
    public List<CityDto> getAll();

    // getById
    public CityDto getById(Integer id) throws CityNotFound;

    // getByName
    public CityDto getByName(String name) throws CityNotFound;

    // create
    public CityDto create(CityUpdateDto cityUpdateDto) throws CityIncorrect, CityAlreadyExist;

    // update
    public CityDto update(Integer id, CityUpdateDto cityUpdateDto) throws CityIncorrect, CityNotFound;

    // delete
    public CityDto delete(Integer id) throws CityNotFound;

}
