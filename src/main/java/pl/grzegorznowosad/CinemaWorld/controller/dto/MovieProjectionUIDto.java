package pl.grzegorznowosad.CinemaWorld.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieProjectionUIDto {

    private Integer id;
    private Integer roomId;
    private String movie;
    private String city;
    private String date;
    private String timeFrom;
    private String timeTo;


}
