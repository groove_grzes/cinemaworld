package pl.grzegorznowosad.CinemaWorld.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoleApplicationCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoleApplicationDto;
import pl.grzegorznowosad.CinemaWorld.controller.interfaces.RoleApplicationController;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationAlreadyExists;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationDataIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.RoleApplicationService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class RoleApplicationControllerImp implements RoleApplicationController {

    @Autowired
    private RoleApplicationService roleApplicationService;

    @Override
    @GetMapping("roleApplication")
    public List<RoleApplicationDto> getAll() {
        return roleApplicationService.getAll();
    }

    @Override
    @GetMapping("roleApplication/{id}")
    public RoleApplicationDto getById(@PathVariable Integer id) throws RoleApplicationNotFound {
        return roleApplicationService.getById(id);
    }

    @Override
    @GetMapping("roleApplication/name/{roleName}")
    public RoleApplicationDto getByRoleName(@PathVariable String roleName) throws RoleApplicationNotFound {
        return roleApplicationService.getByRoleName(roleName);
    }

    @Override
    @PostMapping("roleApplication")
    public RoleApplicationDto create(@RequestBody RoleApplicationCreateDto roleApplicationCreateDto) throws RoleApplicationDataIncorrect, RoleApplicationAlreadyExists {
        return roleApplicationService.create(roleApplicationCreateDto);
    }

    @Override
    @PutMapping("roleApplication/{id}")
    public RoleApplicationDto update(@PathVariable Integer id, @RequestBody RoleApplicationCreateDto roleApplicationCreateDto) throws RoleApplicationNotFound, RoleApplicationDataIncorrect, RoleApplicationAlreadyExists {
        return roleApplicationService.update(id, roleApplicationCreateDto);
    }

    @Override
    @DeleteMapping("roleApplication/{id}")
    public RoleApplicationDto delete(@PathVariable Integer id) throws RoleApplicationNotFound {
        return roleApplicationService.delete(id);
    }
}
