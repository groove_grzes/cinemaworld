package pl.grzegorznowosad.CinemaWorld.service.converter;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieProjectionCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieProjectionDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieProjectionUIDto;
import pl.grzegorznowosad.CinemaWorld.model.City;
import pl.grzegorznowosad.CinemaWorld.model.Movie;
import pl.grzegorznowosad.CinemaWorld.model.MovieProjection;
import pl.grzegorznowosad.CinemaWorld.model.Room;

@Component
public class MovieProjectionDtoConverter {

    public MovieProjectionDto toDto(MovieProjection movieProjection){
        return new MovieProjectionDto(movieProjection.getId(), movieProjection.getRoom().getName(), movieProjection.getMovie().getTitle(), movieProjection.getCity().getId(), movieProjection.getDate(), movieProjection.getTimeFrom(), movieProjection.getTimeTo());
    }

    public MovieProjection toModel(MovieProjectionCreateDto movieProjectionCreateDto, Room room, Movie movie, City city){
        return new MovieProjection(null, movieProjectionCreateDto.getDate(), movieProjectionCreateDto.getTimeFrom(), movieProjectionCreateDto.getTimeTo(), room, movie, city, null);
    }

    public MovieProjectionUIDto toUIDto(MovieProjection movieProjection){
        return new MovieProjectionUIDto(movieProjection.getId(), movieProjection.getRoom().getId(), movieProjection.getMovie().getTitle(), movieProjection.getCity().getName(), movieProjection.getDate(), movieProjection.getTimeFrom(), movieProjection.getTimeTo());
    }


}
