package pl.grzegorznowosad.CinemaWorld.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.grzegorznowosad.CinemaWorld.model.City;

import java.util.List;
import java.util.Optional;

@Repository
public interface CityRepository extends CrudRepository<City, Integer> {

    List<City> findAll();
    Optional<City> findById(Integer id);
    Optional<City> findByName(String name);

}
