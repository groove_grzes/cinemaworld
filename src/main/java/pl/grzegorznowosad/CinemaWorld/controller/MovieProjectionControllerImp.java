package pl.grzegorznowosad.CinemaWorld.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieProjectionCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieProjectionDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationWithStatusDto;
import pl.grzegorznowosad.CinemaWorld.controller.interfaces.MovieProjectionController;
import pl.grzegorznowosad.CinemaWorld.service.MovieProjectionServiceImp;
import pl.grzegorznowosad.CinemaWorld.service.exception.*;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.MovieProjectionService;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1/")
public class MovieProjectionControllerImp implements MovieProjectionController {

    @Autowired
    private MovieProjectionService movieProjectionService;

    @Autowired
    private MovieProjectionServiceImp movieProjectionServiceImp;

    @Override
    @GetMapping("movieProjection")
    public List<MovieProjectionDto> getAll() {
        return movieProjectionService.getAll();
    }

    @Override
    @GetMapping("movieProjection/{id}")
    public MovieProjectionDto getById(@PathVariable Integer id) throws MovieProjectionNotFound {
        return movieProjectionService.getById(id);
    }

    @Override
    @PostMapping("movieProjection")
    public MovieProjectionDto create(@RequestBody MovieProjectionCreateDto movieProjectionCreateDto) throws MovieProjectionIncorrect, CityNotFound, RoomNotFound, MovieNotFound {
        return movieProjectionService.create(movieProjectionCreateDto);
    }

    @Override
    @PutMapping("movieProjection/{id}")
    public MovieProjectionDto update(@PathVariable Integer id, @RequestBody MovieProjectionCreateDto movieProjectionCreateDto) throws MovieProjectionNotFound, MovieProjectionIncorrect, CityNotFound, RoomNotFound, MovieNotFound {
        return movieProjectionService.update(id, movieProjectionCreateDto);
    }

    @Override
    @DeleteMapping("movieProjection/{id}")
    public MovieProjectionDto delete(@PathVariable Integer id) throws MovieProjectionNotFound {
        return movieProjectionService.delete(id);
    }

    @Override
    @GetMapping("movieProjection/{id}/reservations")
    public ReservationWithStatusDto[][] getAllReservations(@PathVariable Integer id) throws MovieProjectionNotFound {
        return movieProjectionService.getAllReservationsForMovieProjection(id);
    }



}
