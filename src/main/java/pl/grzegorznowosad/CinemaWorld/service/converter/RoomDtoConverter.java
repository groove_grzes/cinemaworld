package pl.grzegorznowosad.CinemaWorld.service.converter;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoomCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoomDto;
import pl.grzegorznowosad.CinemaWorld.model.Room;

@Component
public class RoomDtoConverter {

    public Room toModel(RoomCreateDto roomCreateDto){
        return new Room(null, roomCreateDto.getRows(), roomCreateDto.getSeats(), roomCreateDto.getName(), null);
    }

    public RoomDto toDto(Room room){
        return new RoomDto(room.getId(), room.getRows(), room.getSeats(), room.getName());
    }

    public RoomCreateDto toDtoFromVIew(RoomDto roomDto){
        return new RoomCreateDto(roomDto.getId(),roomDto.getRows(), roomDto.getSeats(), roomDto.getName());
    }

}
