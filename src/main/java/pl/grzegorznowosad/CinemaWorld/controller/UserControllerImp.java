package pl.grzegorznowosad.CinemaWorld.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserUpdateDto;
import pl.grzegorznowosad.CinemaWorld.controller.interfaces.UserController;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationNotFound;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserAlreadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.UserService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class UserControllerImp implements UserController {

    @Autowired
    private UserService userService;


    @Override
    @GetMapping("user")
    public List<UserDto> getAll() {
        return userService.getAll();
    }

    @Override
    @GetMapping("user/{id}")
    public UserDto getById(@PathVariable Integer id) throws UserNotFound {
        return userService.getById(id);
    }

    @Override
    @GetMapping("user/login/{login}")
    public UserDto getByLogin(@PathVariable String login) throws UserNotFound {
        return userService.getByLogin(login);
    }

    @Override
    @PostMapping("user")
    public UserDto create(@RequestBody UserCreateDto userCreateDto) throws UserAlreadyExist, UserIncorrect, RoleApplicationNotFound {
        return userService.create(userCreateDto);
    }

    @Override
    @PutMapping("user/{id}")
    public UserDto update(@PathVariable Integer id, @RequestBody UserUpdateDto userUpdateDto) throws UserNotFound, UserIncorrect, RoleApplicationNotFound {
        return userService.update(id, userUpdateDto);
    }

    @Override
    @DeleteMapping("user/{id}")
    public UserDto delete(@PathVariable Integer id) throws UserNotFound {
        return userService.delete(id);
    }
}
