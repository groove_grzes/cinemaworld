package pl.grzegorznowosad.CinemaWorld.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieCategoryCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieCategoryDto;
import pl.grzegorznowosad.CinemaWorld.controller.interfaces.MovieCategoryController;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieCategoryAleadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieCategoryIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieCategoryNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.MovieCategoryService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class MovieCategoryControllerImp implements MovieCategoryController {

    @Autowired
    private MovieCategoryService movieCategoryService;

    @Override
    @GetMapping("movieCategory")
    public List<MovieCategoryDto> getAll() {
        return movieCategoryService.getAll();
    }

    @Override
    @GetMapping("movieCategory/{id}")
    public MovieCategoryDto getById(@PathVariable Integer id) throws MovieCategoryNotFound {
        return movieCategoryService.getById(id);
    }

    @Override
    @GetMapping("movieCategory/name/{name}")
    public MovieCategoryDto getByName(@PathVariable String name) throws MovieCategoryNotFound {
        return movieCategoryService.getByName(name);
    }

    @Override
    @PostMapping("movieCategory")
    public MovieCategoryDto create(@RequestBody MovieCategoryCreateDto movieCategoryCreateDto) throws MovieCategoryIncorrect, MovieCategoryAleadyExist {
        return movieCategoryService.create(movieCategoryCreateDto);
    }

    @Override
    @PutMapping("movieCategory/{id}")
    public MovieCategoryDto update(@PathVariable Integer id, @RequestBody MovieCategoryCreateDto movieCategoryCreateDto) throws MovieCategoryIncorrect, MovieCategoryNotFound {
        return movieCategoryService.update(id, movieCategoryCreateDto);
    }

    @Override
    @DeleteMapping("movieCategory/{id}")
    public MovieCategoryDto delete(@PathVariable Integer id) throws MovieCategoryNotFound {
        return movieCategoryService.delete(id);
    }
}
