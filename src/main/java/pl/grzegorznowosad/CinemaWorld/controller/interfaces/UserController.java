package pl.grzegorznowosad.CinemaWorld.controller.interfaces;

import pl.grzegorznowosad.CinemaWorld.controller.dto.UserCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserUpdateDto;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationNotFound;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserAlreadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserNotFound;

import java.util.List;

public interface UserController {

    // getAll
    public List<UserDto> getAll();

    // getById
    public UserDto getById(Integer id) throws UserNotFound;

    // getByLogin
    public UserDto getByLogin(String login) throws UserNotFound;

    // create
    public UserDto create(UserCreateDto userCreateDto) throws UserAlreadyExist, UserIncorrect, RoleApplicationNotFound;

    // update
    public UserDto update(Integer id, UserUpdateDto userUpdateDto) throws UserNotFound, UserIncorrect, RoleApplicationNotFound;

    // delete
    public UserDto delete(Integer id) throws UserNotFound;
}
