package pl.grzegorznowosad.CinemaWorld.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MovieDto {

    private Integer id;
    private String title;
    private String director;
    private Integer year;
    private Integer movieCategoryId;

}
