# **CinemaWorld - Your world of cinema**

#### **1. Overview**

CinemaWorld is a system for selling cinema tickets.

#### **2. Goal**

Business goal: building application for selling cinema tickets

Personal goal: 
- building full stack technology application (backend, frontend)
- Backend - building enterprise application
- Frontend - Javascript (ES6), UI, fully responsive for mobile

<img src="http://www.grzegorznowosad.pl:85/pic/help/projection1.JPG" height="50%" width="25%"  />
 <img src="http://www.grzegorznowosad.pl:85/pic/help/projection2.JPG" height="50%" width="25%" />
 <img src="http://www.grzegorznowosad.pl:85/pic/help/projection3.JPG" height="50%" width="25%" />

#### **3. Author**

You can find more information about me on my website (www.grzegorznowosad.pl)

#### **4. Technology stack**
Some of the main technologies that were used:

Backend:
- Java Core
- Java 8
- Spring framework (Boot, Core, MVC, Security)
- JPA
- Hibernate
- MySQL

Frontend:
- JavaScript (ES6)
- HTML
- CSS
- Thymeleaf
- Bootstrap 4

and additionally some of every-day tools like

- IntelliJ
- VisualStudio Code
- Maven
- Git

#### **5. Testing**

TBA


#### **6. How to use CinemaWorld**

- login in, registering

First you need to sign in to CinemaWorld. For not logged users only login page is available.
To sign in into the service use Your credentials or register for Your own new account.

Please provide Your real email address, as to this address tickets will be send :-)

If Your role in forum is admin or GOD, You have access to admin panel, where You can find possibilities to manage (ADD, MODIFY, DELETE) cinema resources:

    - city
    - room
    - movie
    - movie category
    - movie projection
    - user (only delete !)
    
Thank You for spending time using CinemaWorld. I really appreciate feedback so please do not hestitate to share with me Your thoughts and suggestions.