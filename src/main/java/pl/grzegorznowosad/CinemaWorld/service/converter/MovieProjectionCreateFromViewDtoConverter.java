package pl.grzegorznowosad.CinemaWorld.service.converter;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieProjectionCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieProjectionCreateFromViewDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieProjectionDto;

@Component
public class MovieProjectionCreateFromViewDtoConverter {

    public MovieProjectionCreateFromViewDto toEditDto(MovieProjectionDto movieProjectionDto, Integer id, String city){
        return new MovieProjectionCreateFromViewDto(id, movieProjectionDto.getRoomName(), movieProjectionDto.getMovieTitle(), city, movieProjectionDto.getDate(), movieProjectionDto.getTimeFrom(), movieProjectionDto.getTimeTo() );
    }

    public MovieProjectionCreateDto toModel(MovieProjectionCreateFromViewDto movieProjectionCreateFromViewDto){
        return new MovieProjectionCreateDto(movieProjectionCreateFromViewDto.getRoom(), movieProjectionCreateFromViewDto.getMovie(), movieProjectionCreateFromViewDto.getCity(), movieProjectionCreateFromViewDto.getDate(), movieProjectionCreateFromViewDto.getTimeFrom(), movieProjectionCreateFromViewDto.getTimeTo());
    }
}
