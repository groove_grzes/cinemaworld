package pl.grzegorznowosad.CinemaWorld.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoomDto {

    private Integer id;
    private Integer rows;
    private Integer seats;
    private String name;

}
