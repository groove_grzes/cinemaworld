package pl.grzegorznowosad.CinemaWorld.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserUpdateDto;
import pl.grzegorznowosad.CinemaWorld.model.RoleApplication;
import pl.grzegorznowosad.CinemaWorld.model.User;
import pl.grzegorznowosad.CinemaWorld.model.UserRole;
import pl.grzegorznowosad.CinemaWorld.repository.RoleApplicationRepository;
import pl.grzegorznowosad.CinemaWorld.repository.UserRepository;
import pl.grzegorznowosad.CinemaWorld.repository.UserRoleRepository;
import pl.grzegorznowosad.CinemaWorld.service.converter.UserDtoConverter;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationNotFound;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserAlreadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDtoConverter userDtoConverter;

    @Autowired
    private RoleApplicationRepository roleApplicationRepository;

    @Override
    public List<UserDto> getAll() {

        List<User> all = userRepository.findAll();
        List<UserDto> allUsersDto = new ArrayList<>();

        all.forEach(x -> {
            allUsersDto.add(userDtoConverter.toDto(x));
        });

        return allUsersDto;
    }

    @Override
    public UserDto getById(Integer id) throws UserNotFound {

        Optional<User> byId = userRepository.findById(id);

        if(byId.isPresent()){

            User user = byId.get();

            return userDtoConverter.toDto(user);

        }else{
            throw new UserNotFound();
        }
    }

    @Override
    public UserDto getByLogin(String login) throws UserNotFound {

        Optional<User> byLogin = userRepository.findByLogin(login);

        if(byLogin.isPresent()){

            User user = byLogin.get();

            return userDtoConverter.toDto(user);

        }else{
            throw new UserNotFound();
        }
    }

    @Override
    public UserDto create(UserCreateDto userCreateDto) throws UserIncorrect, UserAlreadyExist, RoleApplicationNotFound {

        if(userCreateDto.getLogin() == null || userCreateDto.getPassword() == null){
            throw new UserIncorrect();
        }

        if(userCreateDto.getLogin().isEmpty() || userCreateDto.getPassword().isEmpty()){
            throw new UserIncorrect();
        }

        if(userRepository.findByLogin(userCreateDto.getLogin()).isPresent()){
            throw new UserAlreadyExist();
        }

        Optional<RoleApplication> client = roleApplicationRepository.findByRoleName("client");

        if(client.isPresent()){

            RoleApplication roleApplication = client.get();

            String password = userCreateDto.getPassword();

            BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
            String encryptedPassword = bcrypt.encode(password);

            userCreateDto.setPassword(encryptedPassword);

            User userSaved = userRepository.save(userDtoConverter.toModel(userCreateDto, roleApplication));

            return userDtoConverter.toDto(userSaved);

        }else{
            throw new RoleApplicationNotFound();
        }
    }

    @Override
    public UserDto update(Integer id, UserUpdateDto userUpdateDto) throws UserNotFound, UserIncorrect, RoleApplicationNotFound {

        if(userUpdateDto.getPassword() == null){
            throw new UserIncorrect();
        }

        Optional<User> byId = userRepository.findById(id);
        Optional<RoleApplication> byRoleName = roleApplicationRepository.findByRoleName(userUpdateDto.getRole());

        if(byId.isPresent()){

            if(byRoleName.isPresent()){

                User user = byId.get();
                RoleApplication roleApplication = byRoleName.get();

                user.setPassword(userUpdateDto.getPassword());
                user.setRoleApplication(roleApplication);

                User userUpdated = userRepository.save(user);

                return userDtoConverter.toDto(userUpdated);

            }else{
                throw new RoleApplicationNotFound();
            }



        }else{
            throw new UserNotFound();
        }
    }

    @Override
    public UserDto delete(Integer id) throws UserNotFound {

        Optional<User> byId = userRepository.findById(id);

        if(byId.isPresent()){
            User user = byId.get();

            userRepository.delete(user);

            return userDtoConverter.toDto(user);
        }else{
            throw new UserNotFound();
        }
    }

    @Override
    public UserDto changeRole(Integer id, String role) throws UserNotFound, RoleApplicationNotFound {
        Optional<User> byId = userRepository.findById(id);
        Optional<RoleApplication> byRoleName = roleApplicationRepository.findByRoleName(role);

        if(byId.isPresent()){

            if(byRoleName.isPresent()){

                User user = byId.get();
                RoleApplication roleApplication = byRoleName.get();

                user.setRoleApplication(roleApplication);

                User useWithRoleChanged = userRepository.save(user);

                return userDtoConverter.toDto(useWithRoleChanged);

            }else{
                throw new RoleApplicationNotFound();
            }
        }else{
            throw new UserNotFound();
        }
    }
}
