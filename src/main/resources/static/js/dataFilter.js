function searchContent(wyszukiwarka) {
    var input, filter, ul, li, i, a;

    // input = document.getElementById("mySearch1");
    input = wyszukiwarka;
    filter = input.value.toUpperCase();
    ul = document.getElementById("lista");
    li = ul.getElementsByTagName("li");

    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

function searchInTable(wyszukiwarka) {
    var input, filter, tabela, i;

    input = wyszukiwarka;
    filter = input.value.toUpperCase();

    tabela = document.getElementById("myTable");
    tr = tabela.getElementsByTagName("tr");

    for (i = 1; i < tr.length; i++) {
        var pojedynczy = tr[i].getElementsByTagName("td")[0];

        if (pojedynczy.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }
    }
}