package pl.grzegorznowosad.CinemaWorld.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.CinemaWorld.controller.dto.CityDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.CityUpdateDto;
import pl.grzegorznowosad.CinemaWorld.model.City;
import pl.grzegorznowosad.CinemaWorld.repository.CityRepository;
import pl.grzegorznowosad.CinemaWorld.service.converter.CityDtoConverter;
import pl.grzegorznowosad.CinemaWorld.service.exception.CityAlreadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.CityIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.CityNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.CityService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CityServiceImp implements CityService {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private CityDtoConverter cityDtoConverter;

    @Override
    public List<CityDto> getAll() {
        List<City> all = cityRepository.findAll();
        List<CityDto> allCityDto = new ArrayList<>();

        all.forEach(x -> {
            allCityDto.add(cityDtoConverter.toDto(x));
        });

        return allCityDto;
    }

    @Override
    public CityDto getById(Integer id) throws CityNotFound {
        Optional<City> byId = cityRepository.findById(id);

        if(byId.isPresent()){

            City city = byId.get();

            return cityDtoConverter.toDto(city);

        }else{
            throw new CityNotFound();
        }
    }

    @Override
    public CityDto getByName(String name) throws CityNotFound {
        Optional<City> byName = cityRepository.findByName(name);

        if(byName.isPresent()){
            City city = byName.get();

            return cityDtoConverter.toDto(city);
        }else{
            throw new CityNotFound();
        }
    }

    @Override
    public CityDto create(CityUpdateDto cityUpdateDto) throws CityIncorrect, CityAlreadyExist {
        if(cityUpdateDto.getName() == null){
            throw new CityIncorrect();
        }

        if(cityRepository.findByName(cityUpdateDto.getName()).isPresent()){
            throw new CityAlreadyExist();
        }

        City savedCity = cityRepository.save(cityDtoConverter.toModel(cityUpdateDto));

        return cityDtoConverter.toDto(savedCity);
    }

    @Override
    public CityDto update(Integer id, CityUpdateDto cityUpdateDto) throws CityIncorrect, CityNotFound {
        if(cityUpdateDto.getName() == null){
            throw new CityIncorrect();
        }

        Optional<City> byId = cityRepository.findById(id);

        if(byId.isPresent()){

            City city = byId.get();

            city.setName(cityUpdateDto.getName());

            City updatedCity = cityRepository.save(city);

            return cityDtoConverter.toDto(updatedCity);

        }else{
            throw new CityNotFound();
        }
    }

    @Override
    public CityDto delete(Integer id) throws CityNotFound {
        Optional<City> byId = cityRepository.findById(id);

        if(byId.isPresent()){
            City city = byId.get();

            cityRepository.delete(city);

            return cityDtoConverter.toDto(city);
        }else{
            throw new CityNotFound();
        }
    }
}
