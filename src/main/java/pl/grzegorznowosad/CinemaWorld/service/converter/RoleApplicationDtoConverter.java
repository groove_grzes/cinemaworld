package pl.grzegorznowosad.CinemaWorld.service.converter;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoleApplicationCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoleApplicationDto;
import pl.grzegorznowosad.CinemaWorld.model.RoleApplication;

@Component
public class RoleApplicationDtoConverter {

    public RoleApplication toModel(RoleApplicationCreateDto roleApplicationCreateDto){
        return new RoleApplication(null, roleApplicationCreateDto.getRoleName(), null);
    }

    public RoleApplicationDto toDto(RoleApplication roleApplication){
        return new RoleApplicationDto(roleApplication.getId(), roleApplication.getRoleName());
    }

}
