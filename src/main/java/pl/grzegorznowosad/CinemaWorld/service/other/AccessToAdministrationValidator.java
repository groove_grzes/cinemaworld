package pl.grzegorznowosad.CinemaWorld.service.other;

import org.springframework.stereotype.Component;

@Component
public class AccessToAdministrationValidator {

    public boolean canAccessToAdministration(String role){
        return role.equals("ADMIN") | role.equals("OWNER");
    }

    public boolean isOwner(String role){
        return role.equals("OWNER");
    }
}
