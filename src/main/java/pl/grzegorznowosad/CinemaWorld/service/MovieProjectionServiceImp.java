package pl.grzegorznowosad.CinemaWorld.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.CinemaWorld.controller.dto.*;
import pl.grzegorznowosad.CinemaWorld.model.*;
import pl.grzegorznowosad.CinemaWorld.repository.CityRepository;
import pl.grzegorznowosad.CinemaWorld.repository.MovieProjectionRepository;
import pl.grzegorznowosad.CinemaWorld.repository.MovieRepository;
import pl.grzegorznowosad.CinemaWorld.repository.RoomRepository;
import pl.grzegorznowosad.CinemaWorld.service.converter.MovieProjectionDtoConverter;
import pl.grzegorznowosad.CinemaWorld.service.exception.*;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.MovieProjectionService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MovieProjectionServiceImp  implements MovieProjectionService {

    @Autowired
    private MovieProjectionRepository movieProjectionRepository;

    @Autowired
    private MovieProjectionDtoConverter movieProjectionDtoConverter;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private CityRepository cityRepository;


    @Override
    public List<MovieProjectionDto> getAll() {
        List<MovieProjection> all = movieProjectionRepository.findAll();
        List<MovieProjectionDto> allMovieProjectsDto = new ArrayList<>();

        all.forEach(x -> {
            allMovieProjectsDto.add(movieProjectionDtoConverter.toDto(x));
        });

        return allMovieProjectsDto;
    }

    @Override
    public MovieProjectionDto getById(Integer id) throws MovieProjectionNotFound {
        Optional<MovieProjection> byId = movieProjectionRepository.findById(id);

        if (byId.isPresent()) {
            MovieProjection movieProjection = byId.get();

            return movieProjectionDtoConverter.toDto(movieProjection);
        } else {
            throw new MovieProjectionNotFound();
        }
    }

    @Override
    public MovieProjectionDto create(MovieProjectionCreateDto movieProjectionCreateDto) throws MovieProjectionIncorrect, RoomNotFound, MovieNotFound, CityNotFound {

        // room, movie, city

        Optional<Room> roomByName = roomRepository.findByName(movieProjectionCreateDto.getRoom());
        Optional<Movie> movieByTitle = movieRepository.findByTitle(movieProjectionCreateDto.getMovie());
        Optional<City> cityByName = cityRepository.findByName(movieProjectionCreateDto.getCity());

        if (roomByName.isPresent()) {

            if (movieByTitle.isPresent()) {

                if (cityByName.isPresent()) {

                    Room room = roomByName.get();
                    Movie movie = movieByTitle.get();
                    City city = cityByName.get();

                    MovieProjection movieProjectionSaved = movieProjectionRepository.save(movieProjectionDtoConverter.toModel(movieProjectionCreateDto, room, movie, city));

                    return movieProjectionDtoConverter.toDto(movieProjectionSaved);

                } else {
                    throw new CityNotFound();
                }
            } else {
                throw new MovieNotFound();
            }
        } else {
            throw new RoomNotFound();
        }
    }

    @Override
    public MovieProjectionDto update(Integer id, MovieProjectionCreateDto movieProjectionCreateDto) throws MovieProjectionNotFound, MovieProjectionIncorrect, CityNotFound, MovieNotFound, RoomNotFound {

        Optional<MovieProjection> movieProjectionById = movieProjectionRepository.findById(id);

        Optional<Room> roomByName = roomRepository.findByName(movieProjectionCreateDto.getRoom());
        Optional<Movie> movieByTitle = movieRepository.findByTitle(movieProjectionCreateDto.getMovie());
        Optional<City> cityByName = cityRepository.findByName(movieProjectionCreateDto.getCity());

        if(movieProjectionById.isPresent()){

            if (roomByName.isPresent()) {

                if (movieByTitle.isPresent()) {

                    if (cityByName.isPresent()) {

                        MovieProjection movieProjection = movieProjectionById.get();
                        Room room = roomByName.get();
                        Movie movie = movieByTitle.get();
                        City city = cityByName.get();

                        movieProjection.setCity(city);
                        movieProjection.setRoom(room);
                        movieProjection.setMovie(movie);
                        movieProjection.setDate(movieProjectionCreateDto.getDate());
                        movieProjection.setTimeFrom(movieProjectionCreateDto.getTimeFrom());
                        movieProjection.setTimeTo(movieProjectionCreateDto.getTimeTo());

                        MovieProjection editedMovieProjection = movieProjectionRepository.save(movieProjection);

                        return movieProjectionDtoConverter.toDto(editedMovieProjection);

                    } else {
                        throw new CityNotFound();
                    }
                } else {
                    throw new MovieNotFound();
                }
            } else {
                throw new RoomNotFound();
            }

        }else{
            throw new MovieProjectionNotFound();
        }
    }

    @Override
    public MovieProjectionDto delete(Integer id) throws MovieProjectionNotFound {

        Optional<MovieProjection> byId = movieProjectionRepository.findById(id);

        if (byId.isPresent()) {
            MovieProjection movieProjection = byId.get();

            movieProjectionRepository.delete(movieProjection);

            return movieProjectionDtoConverter.toDto(movieProjection);
        } else {
            throw new MovieProjectionNotFound();
        }
    }

    public List<MovieProjectionUIDto> allMovieProjectionsToUIByCity(Integer cityId) throws CityNotFound {

        Optional<City> cityById = cityRepository.findById(cityId);

        if (cityById.isPresent()) {

            City city = cityById.get();

            List<MovieProjection> allByCity = movieProjectionRepository.findAllByCity(city);
            List<MovieProjectionUIDto> allMovieProjectionsToUi = new ArrayList<>();

            allByCity.forEach(x -> {
                allMovieProjectionsToUi.add(movieProjectionDtoConverter.toUIDto(x));
            });

            return allMovieProjectionsToUi;

        } else {
            throw new CityNotFound();
        }
    }

    @Override
    public ReservationWithStatusDto[][] getAllReservationsForMovieProjection(Integer id) throws MovieProjectionNotFound {

        Optional<MovieProjection> movieProjectionById = movieProjectionRepository.findById(id);

        if (movieProjectionById.isPresent()) {
            MovieProjection movieProjection = movieProjectionById.get();

            int roomRows = movieProjection.getRoom().getRows();
            int roomSeat = movieProjection.getRoom().getSeats();

            ReservationWithStatusDto tablica[][] = new ReservationWithStatusDto[roomRows][roomSeat];

            for (int i = 0; i < tablica.length; i++) {
                for (int j = 0; j < tablica[i].length; j++) {
                    ReservationWithStatusDto reservationWithStatusDto = new ReservationWithStatusDto(false);
                    tablica[i][j] = new ReservationWithStatusDto(false);
                }
            }

            List<Reservation> allReservations = movieProjection.getAllReservations();

            allReservations.forEach(x -> {
                tablica[x.getRow()][x.getSeat()] = new ReservationWithStatusDto(true);
            });


            return tablica;
        } else {
            throw new MovieProjectionNotFound();
        }
    }
}
