package pl.grzegorznowosad.CinemaWorld.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieProjectionDto {

    private Integer id;
    private String roomName;
    private String movieTitle;
    private Integer cityId;
    private String date;
    private String timeFrom;
    private String timeTo;


}
