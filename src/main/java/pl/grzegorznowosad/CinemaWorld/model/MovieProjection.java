package pl.grzegorznowosad.CinemaWorld.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MovieProjection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String date;
    private String timeFrom;
    private String timeTo;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "roomId")
    @ToString.Exclude
    private Room room;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "movieId")
    @ToString.Exclude
    private Movie movie;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn
    @ToString.Exclude
    private City city;

    @OneToMany(mappedBy = "movieProjection", cascade = CascadeType.ALL)
    @ToString.Exclude
    private List<Reservation> allReservations;

}
