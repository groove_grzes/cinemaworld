package pl.grzegorznowosad.CinemaWorld.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer rows;
    private Integer seats;
    private String name;

    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL)
    private List<MovieProjection> allMovieProjections;

}
