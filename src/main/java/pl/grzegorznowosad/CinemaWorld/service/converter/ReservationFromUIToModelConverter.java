package pl.grzegorznowosad.CinemaWorld.service.converter;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationFromViewDto;
import pl.grzegorznowosad.CinemaWorld.model.MovieProjection;
import pl.grzegorznowosad.CinemaWorld.model.Reservation;
import pl.grzegorznowosad.CinemaWorld.model.User;

@Component
public class ReservationFromUIToModelConverter {

    public Reservation toModel(ReservationFromViewDto reservationFromViewDto, User user, MovieProjection movieProjection){
        return new Reservation(null, reservationFromViewDto.getRow(), reservationFromViewDto.getSeat(), user, movieProjection);
    }

}
