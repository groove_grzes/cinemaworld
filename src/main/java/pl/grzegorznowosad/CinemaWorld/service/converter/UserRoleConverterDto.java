package pl.grzegorznowosad.CinemaWorld.service.converter;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserRoleCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserRoleDto;
import pl.grzegorznowosad.CinemaWorld.model.UserRole;

@Component
public class UserRoleConverterDto {

    public UserRole toModel(UserRoleCreateDto userRoleCreateDto){
        return new UserRole(null, userRoleCreateDto.getRole());
    }

    public UserRoleDto toDto(UserRole userRole){
        return new UserRoleDto(userRole.getId(), userRole.getRole());
    }

}
