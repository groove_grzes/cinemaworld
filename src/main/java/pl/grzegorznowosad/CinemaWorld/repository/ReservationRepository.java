package pl.grzegorznowosad.CinemaWorld.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.grzegorznowosad.CinemaWorld.model.MovieProjection;
import pl.grzegorznowosad.CinemaWorld.model.Reservation;
import pl.grzegorznowosad.CinemaWorld.model.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReservationRepository extends CrudRepository<Reservation, Integer> {

    List<Reservation> findAll();
    Optional<Reservation> findById(Integer id);
    List<Reservation> findAllByMovieProjection(MovieProjection movieProjection);
    List<Reservation> findAllByUser(User user);

}
