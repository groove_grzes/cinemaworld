package pl.grzegorznowosad.CinemaWorld.controller.interfaces;

import pl.grzegorznowosad.CinemaWorld.controller.dto.RoleApplicationCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoleApplicationDto;
import pl.grzegorznowosad.CinemaWorld.model.RoleApplication;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationAlreadyExists;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationDataIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationNotFound;

import java.util.List;

public interface RoleApplicationController {

    // getAll
    public List<RoleApplicationDto> getAll();

    // getById
    public RoleApplicationDto getById(Integer id) throws RoleApplicationNotFound;

    // getByRoleName
    public RoleApplicationDto getByRoleName(String roleName) throws RoleApplicationNotFound;

    // create
    public RoleApplicationDto create(RoleApplicationCreateDto roleApplicationCreateDto) throws RoleApplicationDataIncorrect, RoleApplicationAlreadyExists;

    // update
    public RoleApplicationDto update(Integer id, RoleApplicationCreateDto roleApplicationCreateDto) throws RoleApplicationNotFound, RoleApplicationDataIncorrect, RoleApplicationAlreadyExists;

    // delete
    public RoleApplicationDto delete(Integer id) throws RoleApplicationNotFound;

}
