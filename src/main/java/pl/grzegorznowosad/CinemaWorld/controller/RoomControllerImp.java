package pl.grzegorznowosad.CinemaWorld.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoomCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoomDto;
import pl.grzegorznowosad.CinemaWorld.controller.interfaces.RoomController;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoomIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoomNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.RoomService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class RoomControllerImp implements RoomController {

    @Autowired
    private RoomService roomService;


    @Override
    @GetMapping("room")
    public List<RoomDto> getAll() {
        return roomService.getAll();
    }

    @Override
    @GetMapping("room/{id}")
    public RoomDto getById(@PathVariable Integer id) throws RoomNotFound {
        return roomService.getById(id);
    }

    @Override
    @GetMapping("room/name/{name}")
    public RoomDto getByName(@PathVariable String name) throws RoomNotFound {
        return roomService.getByName(name);
    }

    @Override
    @PostMapping("room")
    public RoomDto create(@RequestBody RoomCreateDto roomCreateDto) throws RoomIncorrect {
        return roomService.create(roomCreateDto);
    }

    @Override
    @PutMapping("room/{id}")
    public RoomDto update(@PathVariable Integer id, @RequestBody RoomCreateDto roomCreateDto) throws RoomIncorrect, RoomNotFound {
        return roomService.update(id, roomCreateDto);
    }

    @Override
    @DeleteMapping("room/{id}")
    public RoomDto delete(@PathVariable Integer id) throws RoomNotFound {
        return roomService.delete(id);
    }
}
