package pl.grzegorznowosad.CinemaWorld.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.grzegorznowosad.CinemaWorld.model.City;
import pl.grzegorznowosad.CinemaWorld.model.MovieProjection;

import java.util.List;
import java.util.Optional;

@Repository
public interface MovieProjectionRepository extends CrudRepository<MovieProjection, Integer> {

    List<MovieProjection> findAll();
    Optional<MovieProjection> findById(Integer id);
    List<MovieProjection> findAllByCity(City city);

}
