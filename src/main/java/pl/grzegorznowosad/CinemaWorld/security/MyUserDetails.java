package pl.grzegorznowosad.CinemaWorld.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.CinemaWorld.model.User;
import pl.grzegorznowosad.CinemaWorld.repository.UserRepository;
import pl.grzegorznowosad.CinemaWorld.repository.UserRoleRepository;

import java.util.Optional;

@Service
public class MyUserDetails implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userLogin) throws UsernameNotFoundException {

        Optional<User> byLogin = userRepository.findByLogin(userLogin);

        if(byLogin.isPresent()){

            User user = byLogin.get();

            return org.springframework.security.core.userdetails.User
                    .withUsername(user.getLogin())
                    .password(user.getPassword())
                    .authorities("ROLE_" + user.getRoleApplication().getRoleName())
                    .build();
        }else{
            throw new UsernameNotFoundException("invalid login, user not found");
        }
    }
}
