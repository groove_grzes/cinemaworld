package pl.grzegorznowosad.CinemaWorld.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationFromViewDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationPackFromViewDto;
import pl.grzegorznowosad.CinemaWorld.controller.interfaces.ReservationController;
import pl.grzegorznowosad.CinemaWorld.service.exception.MovieProjectionNotFound;
import pl.grzegorznowosad.CinemaWorld.service.exception.ReservationIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.ReservationNotFound;
import pl.grzegorznowosad.CinemaWorld.service.exception.UserNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.ReservationService;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1/")
public class ReservationControllerImp implements ReservationController {

    @Autowired
    private ReservationService reservationService;

    @Override
    @GetMapping("reservation")
    public List<ReservationDto> getAll() {
        return reservationService.getAll();
    }

    @Override
    @GetMapping("reservation/{id}")
    public ReservationDto getById(@PathVariable Integer id) throws ReservationNotFound {
        return reservationService.getById(id);
    }

    @Override
    @PostMapping("reservation")
    public ReservationDto create(@RequestBody ReservationCreateDto reservationCreateDto) throws ReservationIncorrect, UserNotFound, MovieProjectionNotFound {
        return reservationService.create(reservationCreateDto);
    }

    @Override
    @PutMapping("reservation/{id}")
    public ReservationDto update(@PathVariable Integer id, @RequestBody ReservationCreateDto reservationCreateDto) throws ReservationIncorrect, ReservationNotFound {
        return reservationService.update(id, reservationCreateDto);
    }

    @Override
    @DeleteMapping("reservation/{id}")
    public ReservationDto delete(@PathVariable Integer id) throws ReservationNotFound {
        return reservationService.delete(id);
    }

}
