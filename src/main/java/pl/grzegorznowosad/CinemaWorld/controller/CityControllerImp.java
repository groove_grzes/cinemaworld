package pl.grzegorznowosad.CinemaWorld.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.grzegorznowosad.CinemaWorld.controller.dto.CityDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.CityUpdateDto;
import pl.grzegorznowosad.CinemaWorld.controller.interfaces.CityController;
import pl.grzegorznowosad.CinemaWorld.service.exception.CityAlreadyExist;
import pl.grzegorznowosad.CinemaWorld.service.exception.CityIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.CityNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.CityService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class CityControllerImp implements CityController {

    @Autowired
    private CityService cityService;

    @Override
    @GetMapping("city")
    public List<CityDto> getAll() {
        return cityService.getAll();
    }

    @Override
    @GetMapping("city/{id}")
    public CityDto getById(@PathVariable Integer id) throws CityNotFound {
        return cityService.getById(id);
    }

    @Override
    @GetMapping("city/name/{name}")
    public CityDto getByName(@PathVariable String name) throws CityNotFound {
        return cityService.getByName(name);
    }

    @Override
    @PostMapping("city")
    public CityDto create(@RequestBody CityUpdateDto cityUpdateDto) throws CityIncorrect, CityAlreadyExist {
        return cityService.create(cityUpdateDto);
    }

    @Override
    @PutMapping("city/{id}")
    public CityDto update(@PathVariable Integer id, @RequestBody CityUpdateDto cityUpdateDto) throws CityIncorrect, CityNotFound {
        return cityService.update(id, cityUpdateDto);
    }

    @Override
    @DeleteMapping("city/{id}")
    public CityDto delete(@PathVariable Integer id) throws CityNotFound {
        return cityService.delete(id);
    }
}
