package pl.grzegorznowosad.CinemaWorld.service.converter;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.CinemaWorld.controller.dto.CityDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.CityUpdateDto;
import pl.grzegorznowosad.CinemaWorld.model.City;

@Component
public class CityDtoConverter {

    public City toModel(CityUpdateDto cityUpdateDto){
        return new City(null, cityUpdateDto.getName(), null);
    }

    public CityDto toDto(City city){
        return new CityDto(city.getId(),city.getName());
    }

    public CityUpdateDto toUpdateDto(CityDto cityDto){
        return new CityUpdateDto(cityDto.getId(),cityDto.getName());
    }

}
