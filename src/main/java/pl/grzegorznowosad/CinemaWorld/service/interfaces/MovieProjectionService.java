package pl.grzegorznowosad.CinemaWorld.service.interfaces;

import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieProjectionCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieProjectionDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationPackFromViewDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.ReservationWithStatusDto;
import pl.grzegorznowosad.CinemaWorld.service.exception.*;

import java.util.List;

public interface MovieProjectionService {

    // getAll
    public List<MovieProjectionDto> getAll();

    // getById
    public MovieProjectionDto getById(Integer id) throws MovieProjectionNotFound;

    // create
    public MovieProjectionDto create(MovieProjectionCreateDto movieProjectionCreateDto) throws MovieProjectionIncorrect, RoomNotFound, MovieNotFound, CityNotFound;

    // update
    public MovieProjectionDto update(Integer id, MovieProjectionCreateDto movieProjectionCreateDto) throws MovieProjectionNotFound, MovieProjectionIncorrect, CityNotFound, MovieNotFound, RoomNotFound;

    // delete
    public MovieProjectionDto delete(Integer id) throws MovieProjectionNotFound;

    // getAllReservations
    public ReservationWithStatusDto[][] getAllReservationsForMovieProjection(Integer id) throws MovieProjectionNotFound;

}
