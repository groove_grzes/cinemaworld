package pl.grzegorznowosad.CinemaWorld.service.converter;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieCategoryCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.MovieCategoryDto;
import pl.grzegorznowosad.CinemaWorld.model.MovieCategory;

@Component
public class MovieCategoryDtoConverter {

    public MovieCategoryDto toDto(MovieCategory movieCategory){
        return new MovieCategoryDto(movieCategory.getId(), movieCategory.getName());
    }

    public MovieCategory toModel(MovieCategoryCreateDto movieCategoryCreateDto){
        return new MovieCategory(null, movieCategoryCreateDto.getName(), null);
    }

    public MovieCategoryCreateDto toCreateDtoView(MovieCategoryDto movieCategoryDto){
        return new MovieCategoryCreateDto(movieCategoryDto.getName());
    }

    public MovieCategory toModelFromView(MovieCategoryDto movieCategoryDto){
        return new MovieCategory(movieCategoryDto.getId(), movieCategoryDto.getName(), null);
    }
}
