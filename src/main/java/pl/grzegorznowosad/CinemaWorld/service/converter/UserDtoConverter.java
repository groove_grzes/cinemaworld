package pl.grzegorznowosad.CinemaWorld.service.converter;

import org.springframework.stereotype.Component;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.UserDto;
import pl.grzegorznowosad.CinemaWorld.model.RoleApplication;
import pl.grzegorznowosad.CinemaWorld.model.User;

@Component
public class UserDtoConverter {

    public UserDto toDto(User user){
        return new UserDto(user.getId(), user.getLogin(), user.getPassword(), user.getName(), user.getRoleApplication().getRoleName());
    }

    public User toModel(UserCreateDto userCreateDto, RoleApplication roleApplication){
        return new User(null, userCreateDto.getLogin(), userCreateDto.getPassword(), userCreateDto.getName(), null, roleApplication);
    }

}
