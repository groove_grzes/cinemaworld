package pl.grzegorznowosad.CinemaWorld.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoleApplicationCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoleApplicationDto;
import pl.grzegorznowosad.CinemaWorld.model.RoleApplication;
import pl.grzegorznowosad.CinemaWorld.repository.RoleApplicationRepository;
import pl.grzegorznowosad.CinemaWorld.service.converter.RoleApplicationDtoConverter;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationAlreadyExists;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationDataIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationNotFound;
import pl.grzegorznowosad.CinemaWorld.service.interfaces.RoleApplicationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RoleApplicationServiceImp implements RoleApplicationService {

    @Autowired
    private RoleApplicationRepository roleApplicationRepository;

    @Autowired
    public RoleApplicationDtoConverter roleApplicationDtoConverter;


    @Override
    public List<RoleApplicationDto> getAll() {
        List<RoleApplication> allRoleApplications = roleApplicationRepository.findAll();

        List<RoleApplicationDto> allDtos = new ArrayList<>();

        allRoleApplications.forEach(roleApplication -> {
            allDtos.add(roleApplicationDtoConverter.toDto(roleApplication));
        });

        return allDtos;
    }

    @Override
    public RoleApplicationDto getById(Integer id) throws RoleApplicationNotFound {
        Optional<RoleApplication> byId = roleApplicationRepository.findById(id);

        if (byId.isPresent()) {

            RoleApplication roleApplication = byId.get();

            return roleApplicationDtoConverter.toDto(roleApplication);

        } else {
            throw new RoleApplicationNotFound();
        }
    }

    @Override
    public RoleApplicationDto getByRoleName(String roleName) throws RoleApplicationNotFound {

        Optional<RoleApplication> byRoleName = roleApplicationRepository.findByRoleName(roleName);

        if (byRoleName.isPresent()) {

            RoleApplication roleApplication = byRoleName.get();

            return roleApplicationDtoConverter.toDto(roleApplication);

        } else {
            throw new RoleApplicationNotFound();
        }
    }

    @Override
    public RoleApplicationDto create(RoleApplicationCreateDto roleApplicationCreateDto) throws RoleApplicationDataIncorrect, RoleApplicationAlreadyExists {
        if (roleApplicationCreateDto.getRoleName() == null || roleApplicationCreateDto.getRoleName().isEmpty()) {
            throw new RoleApplicationDataIncorrect();
        }

        if (roleApplicationRepository.findByRoleName(roleApplicationCreateDto.getRoleName()).isPresent()) {
            throw new RoleApplicationAlreadyExists();
        }

        RoleApplication savedRoleApplication = roleApplicationRepository.save(roleApplicationDtoConverter.toModel(roleApplicationCreateDto));

        return roleApplicationDtoConverter.toDto(savedRoleApplication);
    }

    @Override
    public RoleApplicationDto update(Integer id, RoleApplicationCreateDto roleApplicationCreateDto) throws RoleApplicationDataIncorrect, RoleApplicationAlreadyExists, RoleApplicationNotFound {
        if (roleApplicationCreateDto.getRoleName() == null || roleApplicationCreateDto.getRoleName().isEmpty()) {
            throw new RoleApplicationDataIncorrect();
        }

        if (roleApplicationRepository.findByRoleName(roleApplicationCreateDto.getRoleName()).isPresent()) {
            throw new RoleApplicationAlreadyExists();
        }

        Optional<RoleApplication> byId = roleApplicationRepository.findById(id);

        if (byId.isPresent()) {

            RoleApplication roleApplication = byId.get();

            roleApplication.setRoleName(roleApplicationCreateDto.getRoleName());

            RoleApplication editedRoleApplication = roleApplicationRepository.save(roleApplication);

            return roleApplicationDtoConverter.toDto(editedRoleApplication);

        } else {
            throw new RoleApplicationNotFound();
        }

    }

    @Override
    public RoleApplicationDto delete(Integer id) throws RoleApplicationNotFound {

        Optional<RoleApplication> byId = roleApplicationRepository.findById(id);

        if (byId.isPresent()) {

            RoleApplication roleApplication = byId.get();

            roleApplicationRepository.delete(roleApplication);

            return roleApplicationDtoConverter.toDto(roleApplication);

        } else {
            throw new RoleApplicationNotFound();
        }
    }

    public List<RoleApplicationDto> getAllWithoutOwner() throws RoleApplicationNotFound {
        List<RoleApplicationDto> allRolesWithoutOwner = getAll();

        RoleApplicationDto ownerRole = getByRoleName("OWNER");

        allRolesWithoutOwner.remove(ownerRole);

        return allRolesWithoutOwner;
    }
}
