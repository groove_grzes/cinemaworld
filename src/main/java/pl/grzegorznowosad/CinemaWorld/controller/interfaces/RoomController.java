package pl.grzegorznowosad.CinemaWorld.controller.interfaces;

import pl.grzegorznowosad.CinemaWorld.controller.dto.RoomCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoomDto;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoomIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoomNotFound;

import java.util.List;

public interface RoomController {

    // getAll
    public List<RoomDto> getAll();

    // getById
    public RoomDto getById(Integer id) throws RoomNotFound;

    // getByName
    public RoomDto getByName(String name) throws RoomNotFound;

    // create
    public RoomDto create(RoomCreateDto roomCreateDto) throws RoomIncorrect;

    // update
    public RoomDto update(Integer id, RoomCreateDto roomCreateDto) throws RoomIncorrect, RoomNotFound;

    // delete
    public RoomDto delete(Integer id) throws RoomNotFound;

}
