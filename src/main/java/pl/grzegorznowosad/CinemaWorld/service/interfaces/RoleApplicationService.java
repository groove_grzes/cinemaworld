package pl.grzegorznowosad.CinemaWorld.service.interfaces;

import pl.grzegorznowosad.CinemaWorld.controller.dto.RoleApplicationCreateDto;
import pl.grzegorznowosad.CinemaWorld.controller.dto.RoleApplicationDto;
import pl.grzegorznowosad.CinemaWorld.model.RoleApplication;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationAlreadyExists;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationDataIncorrect;
import pl.grzegorznowosad.CinemaWorld.service.exception.RoleApplicationNotFound;

import java.util.List;

public interface RoleApplicationService {

    // getAll
    public List<RoleApplicationDto> getAll();

    // getById
    public RoleApplicationDto getById(Integer id) throws RoleApplicationNotFound;

    // getByName
    public RoleApplicationDto getByRoleName(String roleName) throws RoleApplicationNotFound;

    // create
    public RoleApplicationDto create(RoleApplicationCreateDto roleApplicationCreateDto) throws RoleApplicationDataIncorrect, RoleApplicationAlreadyExists;

    // update
    public RoleApplicationDto update(Integer id, RoleApplicationCreateDto roleApplicationCreateDto) throws RoleApplicationDataIncorrect, RoleApplicationAlreadyExists, RoleApplicationNotFound;

    // delete
    public RoleApplicationDto delete(Integer id) throws RoleApplicationNotFound;

    public List<RoleApplicationDto> getAllWithoutOwner() throws RoleApplicationNotFound;


}
