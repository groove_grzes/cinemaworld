// zmienne globalne
var allSeatsInLayout = document.getElementById("listaWszystkichMiejscNaSali");

var ticketsChoosen = [];

var summaryMiddle = document.getElementById("summary");
var summaryFinal = document.getElementById("summary-final");

var myBtn = document.getElementById("sendingButton");

// Generowanie miejsc na sali
// ----------------------------------------

for (var i = 0; i < reservationsArray.length; i++) {
    var firstRow = document.createElement("li");

    var secondRow = document.createElement("ul");

    secondRow.setAttribute("class", "dwa");

    firstRow.appendChild(secondRow);

    allSeatsInLayout.appendChild(firstRow);

    for (var j = 0; j < reservationsArray[i].length; j++) {
        var drugiLi = document.createElement("li");

        drugiLi.setAttribute("id", "r" + i + "s" + j);

        if (reservationsArray[i][j].reserved == true) {
            drugiLi.classList.add("reserved");
        } else {
            drugiLi.setAttribute("onclick", "reserveUnreserve(" + i + "," + j + ")");
        }

        secondRow.appendChild(drugiLi);
    }
}

// ----------------------------------------


// Usuwanie z tablicy wybranych ticketow
function removeTicketFromList(row, seat){
    var indexToBeDeleted;

    for (var i = 0; i < ticketsChoosen.length; i++) {
        if (ticketsChoosen[i].row == row & ticketsChoosen[i].seat === seat) {
            indexToBeDeleted = i;
        }
    }

    delete ticketsChoosen[indexToBeDeleted];

    ticketsChoosen = ticketsChoosen.filter(function (el) {
        return el != null;
    });
}

// ----------------------------------------
// zarezerwowanie albo odrezerwowanie
function reserveUnreserve(row, seat) {
    var wybraneMiejsceNaSali = document.getElementById("r" + row + "s" + seat);

    if (isInArray(row, seat)) {
        // console.warn("usuwam rezerwacje --> row: " + row + ", seat: " + seat);

        removeTicketFromList(row, seat);

        deleteFromSummaryList(row, seat);

        changeReservationPlaceColor(wybraneMiejsceNaSali, "blue");

        summaryReload()

    } else {

        ticketsChoosen.push({ row, seat });

        var nowyLiWPodsumowaniu = document.createElement("li");
        nowyLiWPodsumowaniu.setAttribute("id", "summary-r" + row + "s" + seat);
        nowyLiWPodsumowaniu.setAttribute("onclick", "removeFromSumary(this," + row + "," + seat +")");
        nowyLiWPodsumowaniu.innerHTML = "row: " + (row + 1) + ", seat: " + (seat + 1);
        summaryMiddle.appendChild(nowyLiWPodsumowaniu);

        var liDoOstatniegoPodsumowania = document.createElement("li");
        liDoOstatniegoPodsumowania.setAttribute("id", "summary-final-r" + row + "s" + seat);
        liDoOstatniegoPodsumowania.innerHTML = "row: " + (row + 1) + ", seat: " + (seat + 1);
        summaryFinal.appendChild(liDoOstatniegoPodsumowania);

        changeReservationPlaceColor(wybraneMiejsceNaSali, "orange");

        summaryReload()
    }
}

// ----------------------------------------
// usuniecie z listy summary
function deleteFromSummaryList(row, seat){
    var doUsuniecia = document.getElementById("summary-r" + row + "s" + seat);

    var doUsunieciaFinal = document.getElementById("summary-final-r" + row + "s" + seat);
    // console.warn(doUsuniecia);
    summaryMiddle.removeChild(doUsuniecia);
    summaryFinal.removeChild(doUsunieciaFinal);

}

// zmiana koloru miejsca
function changeReservationPlaceColor(miejsce, kolor){
    miejsce.style.backgroundColor = kolor;
}

// sprawdza czy rezerwacja jest obecnie w tabeli
function isInArray(row, seat) {

    var wynik = false;

    for (var i = 0; i < ticketsChoosen.length; i++) {
        if (ticketsChoosen[i].row == row & ticketsChoosen[i].seat === seat) {
            wynik = true;
        }
    }

    return wynik;
}

// usuwanie z podsumowania przez klikniecie w podsummowaniu
function removeFromSumary(obiektDoUsuniecia, row, seat) {

    summaryMiddle.removeChild(obiektDoUsuniecia);

    var doUsunieciaFinal = document.getElementById("summary-final-r" + row + "s" + seat);
    summaryFinal.removeChild(doUsunieciaFinal);

    var wybraneMiejsceNaSali = document.getElementById("r" + row + "s" + seat);

    removeTicketFromList(row, seat);

    changeReservationPlaceColor(wybraneMiejsceNaSali, "blue");

    summaryReload()

    // sendingButtonChange(ticketsChoosen.length);
}

// aktualizacja podsumowania
function summaryReload() {
    var iloscBiletow = document.getElementById("ilosc");
    iloscBiletow.innerHTML = ticketsChoosen.length;

    var cenaBiletow = document.getElementById("cena");
    cenaBiletow.innerHTML = ticketsChoosen.length * 15;

    sendingButtonChange(ticketsChoosen.length);
}

// zmiana widocznosci przycisku wysylajacego
function sendingButtonChange(arrayLength){
    if(arrayLength === 0){
        myBtn.style.visibility = "hidden";
    }else{
        myBtn.style.visibility = "visible";
    }
}


// wysylanie rezerwacji do API
function sendReservations() {

    var packToBeSend = {
        listOfReservations: ticketsChoosen,
        movieProjectionId: movieId,
        userName: userName
    };

    fetch("/projection/" + movieId + "/makeReservations", {
        // fetch("http://grzegorznowosad.pl:85/projection/" + movieId + "/makeReservations", {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(packToBeSend),
        headers: {
            'Content-Type': 'application/json'
        }
        // }).then(res => res.json())
    }).then(window.location.href = "/success")
    // }).then(window.location.href = "http://grzegorznowosad.pl:85/success")
        .catch(error => window.location.href = "/oops");
    // .catch(error => window.location.href = "http://grzegorznowosad.pl:85/oops");
}




