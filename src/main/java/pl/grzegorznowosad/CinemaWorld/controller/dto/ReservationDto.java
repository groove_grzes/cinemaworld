package pl.grzegorznowosad.CinemaWorld.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReservationDto {

    private Integer id;
    private Integer row;
    private Integer seat;
    private String user;
    private Integer movieProjectionId;

}
